beego 1.9.2
本系统采用前后端分离设计，所以beego用到的功能不多，主要用到了日志、路由、错误处理，beego主要用来写API。


说明：本项目部署除了mysql之外无须任何其他软件，启动时独占服务器一个端口，如果需要和其它项目共用端口，需要使用nginx代理设置，并使用nginx的url重写功能。
output目录结构：
    conf
        app.conf
    data
        judge_data      题目测试数据，这个目录务必不能通过nginx访问到
    run                 用于运行用户提交代码的目录
    public              前端可以直接访问的资源（包括html、js、css、images）
        frontend   这个目录是oj_frontend项目build生成的目录
        images          go语言处理的图片数据
            remoteoj    从remoteoj拉取的图片
            users       用户上传图片
    core_compiler       fineoj本地oj编译程序
    core_executor       fineoj本地oj执行程序
    main                整体项目运行入口