package models

import (
	"github.com/astaxie/beego/logs"
	"fmt"
)

type Problem struct {
	Id int64
	Oj string
	RemoteProblemId string
	Title string
	TimeLimit string
	MemLimit string
	Description string
	Input string
	Output string
	SampleInput string
	SampleOutput string
	Hint string
	AuthorId int
	Public bool
}

func (this *Problem) storeDetailsIntoDBById() {
	rows, err := DB.Query("select id from problems_detail where id=?", this.Id)
	PanicErr(err)
	defer rows.Close()
	if rows.Next() {
		_, err = DB.Exec("update problems_detail set time_limit=?,mem_limit=?,description=?,input=?,output=?,sample_input=?,sample_output=?,hint=? where id=?", this.TimeLimit, this.MemLimit, this.Description, this.Input, this.Output, this.SampleInput, this.SampleOutput, this.Hint, this.Id)
		PanicErr(err)
	} else {
		_, err = DB.Exec("INSERT INTO problems_detail (id, time_limit, mem_limit, description, input, output, sample_input, sample_output, hint) VALUES (?,?,?,?,?,?,?,?,?)", this.Id, this.TimeLimit, this.MemLimit, this.Description, this.Input, this.Output, this.SampleInput, this.SampleOutput, this.Hint)
		if err != nil {
			logs.Warn(err) //可能之前插入过会冲突
		}
	}
}

//如果存在就更新，如果不存在就插入，并更新this.Id
func (this *Problem) StoreFineOjProblem() {
	if this.Oj != "fineoj" {
		panic("OJ type is not fineoj")
	}
	if this.AuthorId <= 0 {
		panic("Author Id <= 0")
	}
	if this.Id > 0 {
		if fmt.Sprint(this.Id) != this.RemoteProblemId {
			panic("Id != RemoteProblemId")
		}
		rows, err := DB.Query("select id from problems where oj=? AND id=? AND remote_problem_id=? AND author=?", this.Oj, this.Id, this.RemoteProblemId, this.AuthorId)
		PanicErr(err)
		defer rows.Close()
		PanicErr(err)
		if !rows.Next() {
			panic("there is not such fineoj problem whose id is "+fmt.Sprintf("%d",this.Id)+" AND remote_problem_id="+this.RemoteProblemId)
		}
		_, err = DB.Exec("update problems set title=?,public=? where id=?", this.Title, this.Public, this.Id)
		PanicErr(err)
	} else {
		if this.RemoteProblemId != "" {
			panic("RemoteProblemId != \"\"")
		}
		var id int64
		ret, err := DB.Exec("INSERT INTO problems (oj,remote_problem_id,title,author,public) VALUES (?,?,?,?,?)", this.Oj, this.RemoteProblemId, this.Title, this.AuthorId, this.Public)
		PanicErr(err)
		id, err = ret.LastInsertId()
		PanicErr(err)
		this.Id = id
		_, err = DB.Exec("update problems set remote_problem_id=? where id=?", this.Id, this.Id)
		PanicErr(err)
		this.RemoteProblemId = fmt.Sprintf("%d", id)
	}
	this.storeDetailsIntoDBById()
}

//如果存在就更新，如果不存在就插入
func (this *Problem) StoreRemoteProblem() {
	if this.Id > 0 {
		panic("Id > 0")
	}
	if this.Oj == "fineoj" {
		panic("this is not remote OJ")
	}
	rows, err := DB.Query("select id from problems where oj=? AND remote_problem_id=?", this.Oj, this.RemoteProblemId)
	PanicErr(err)
	defer rows.Close()
	PanicErr(err)
	var id int64
	if rows.Next() {
		err = rows.Scan(&id)
		PanicErr(err)
		_, err := DB.Exec("update problems set title=?, update_time=CURRENT_TIMESTAMP where id=?",this.Title, id)
		PanicErr(err)
	} else {
		ret, err := DB.Exec("INSERT INTO problems (oj,remote_problem_id,title) VALUES (?,?,?)", this.Oj, this.RemoteProblemId, this.Title)
		if err != nil {
			logs.Warn(err)
			return
		}
		id, err = ret.LastInsertId()
		PanicErr(err)
	}
	this.Id = id
	this.storeDetailsIntoDBById()
}

func (this *Problem)Print() {
	fmt.Println("Oj: "+this.Oj+"*")
	fmt.Println("ProblemId: "+this.RemoteProblemId+"*")
	fmt.Println("Title: "+this.Title+"*")
	fmt.Println("\nTimeLimit: \n"+this.TimeLimit+"*")
	fmt.Println("\nMemLimit: \n"+this.MemLimit+"*")
	fmt.Println("\nDescription: \n"+this.Description+"*")
	fmt.Println("\nInput: \n"+this.Input+"*")
	fmt.Println("\nOutput: \n"+this.Output+"*")
	fmt.Println("\nSampleInput: \n"+this.SampleInput+"*")
	fmt.Println("\nSampleOutput: \n"+this.SampleOutput+"*")
	fmt.Println("\nHint: \n"+this.Hint+"*")
}

func PanicErr(err error) {
	if err != nil {
		logs.Warning(err)
		panic(err)
	}
}

func GetLastedPid(oj string) string{
	rows, err := DB.Query("select remote_problem_id from problems where oj=? order by id desc", oj)
	PanicErr(err)
	defer rows.Close()
	PanicErr(err)
	if rows.Next() {
		var remote_problem_id string
		err = rows.Scan(&remote_problem_id)
		PanicErr(err)
		return remote_problem_id
	} else {
		switch oj {
		case "hdu":
			return "1000"
		case "cf" :
			return "1A"
		default:
			panic("Unknown oj")
		}
	}
}