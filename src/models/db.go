package models

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/astaxie/beego"
	"fmt"
	"time"
	"github.com/astaxie/beego/logs"
)

var DB *sql.DB

func GetNowTimestamp() int64 {
	return time.Now().UnixNano() / 1000000
}

func init() {
	logs.Info("connecting to mysql server...")
	mysqluser := beego.AppConfig.String("mysqluser")
	mysqlpass := beego.AppConfig.String("mysqlpass")
	mysqlurl := beego.AppConfig.String("mysqlurl")
	mysqlport := beego.AppConfig.String("mysqlport")
	mysqldb := beego.AppConfig.String("mysqldb")
	var err error
	DB, err = sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8", mysqluser, mysqlpass, mysqlurl, mysqlport, mysqldb))
	if err != nil {
		panic(err.Error())
	}
	DB.SetMaxOpenConns(20000)
	DB.SetMaxIdleConns(20000)
	err = DB.Ping()
	if err != nil {
		panic(err.Error())
	}
}
