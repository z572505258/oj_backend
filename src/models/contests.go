package models

import "github.com/astaxie/beego/logs"

type ContestProblem struct {
	ProblemId    int64
	ProblemIndex int64
}

type Contest struct {
	Id        int64
	Type      int64
	Title     string
	AuthorId  int
	Public    bool
	StartTime string
	EndTime   string
	Password  string
	Problems  []ContestProblem
}

func (this *Contest) storeDetailsIntoDBById() {
}

//如果存在就更新，如果不存在就插入，并更新this.Id
func (this *Contest) StoreFineOjProblem() {
}

//如果存在就更新，如果不存在就插入
func (this *Contest) StoreRemoteProblem() {
}

func (this Contest) StoreContest() {
	if this.Type != 0 && this.Type != 1 {
		panic("Type != 0 OR 1")
	}
	if len(this.Password) > 20 {
		panic("password too long")
	}
	if len(this.Problems) > 26 {
		panic("contest problems too much!!")
	}
	tx, err := DB.Begin()
	PanicErr(err)
	defer func() {
		if err := recover(); err != nil {
			logs.Warn("mysql rollback !!!!")
			tx.Rollback()
			panic("db error")
		}
	}()
	ret, err := tx.Exec(
		"INSERT INTO contests (type,author,title,start_time,end_time,password) VALUES (?,?,?,?,?,?)",
		this.Type, this.AuthorId, this.Title, this.StartTime, this.EndTime, this.Password)
	PanicErr(err)
	id, err := ret.LastInsertId()
	PanicErr(err)
	this.Id = id
	for i := 0; i < len(this.Problems); i++ {
		ret, err = tx.Exec(
			"INSERT INTO contest_problems (contest_id, problem_id, problem_index) VALUES (?,?,?)",
			this.Id, this.Problems[i].ProblemId, this.Problems[i].ProblemIndex)
		PanicErr(err)
	}
	tx.Commit()
}
