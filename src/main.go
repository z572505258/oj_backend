package main

import (
	"github.com/astaxie/beego"
	_ "./remote"
	//_ "./judger"
	_ "./controllers/problems"
	_ "./controllers/contests"
	_ "./controllers/users"
	_ "./controllers/solutions"
	_ "./controllers/upload"
	_ "./controllers/homepage"
	_ "./controllers/global"
	"os"
)

func main() {
	beego.Run()
}

type IndexController struct {
	beego.Controller
}

var index_bytes []byte

func (this *IndexController) Get() { //响应时间小于0.1ms
	this.Ctx.ResponseWriter.Write(index_bytes)
}

func init() {
	if beego.BConfig.RunMode == beego.PROD {
		//加载index.html到内存中
		f,err := os.Open("public\\frontend\\index.html")
		if err!=nil {
			panic(err)
		}
		defer f.Close()
		var tmp = make([]byte, 10240)
		n,err := f.Read(tmp)
		if err!=nil {
			panic(err)
		}
		index_bytes = tmp[0:n]
		beego.BConfig.EnableErrorsShow = false;
		beego.BConfig.EnableErrorsRender = false;
		beego.Router(beego.AppConfig.String("rootpath") + "/*", &IndexController{}) //如果匹配不到其它path，就返回index.html
		beego.SetStaticPath(beego.AppConfig.String("rootpath") + "/static", "public/frontend/static") //静态资源
	}
	beego.SetStaticPath(beego.AppConfig.String("rootpath") + "/images", "public/images") //静态资源
}
