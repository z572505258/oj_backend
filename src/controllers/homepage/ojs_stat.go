package homepage

import (
	"github.com/astaxie/beego"
	"encoding/json"
	"../../remote/manager"
)

type OjsStat struct {
	beego.Controller
}

func (this *OjsStat) Post() {
	var m struct {
		Id int
	}
	json.Unmarshal(this.Ctx.Input.RequestBody, &m)
	this.Data["json"] = ojs_manager.GetOjsStat()
	this.ServeJSON()
}

func init() {
	beego.Router(beego.AppConfig.String("rootpath")+"/api/ojs_stat", &OjsStat{})
}
