package upload



import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"encoding/json"
	"../../utils"
	"../filters"
	"../../models"
	"os"
	"fmt"
)

type JudgeData struct {
	beego.Controller
}

var temp_dir string

func (this *JudgeData) Post() {
	var res = make(map[string]interface{})
	defer func() {
		err := recover()
		if err!=nil {
			logs.Warn(err)
			res["success"] = false
			res["msg"] = "System Error"
		}
		this.Data["json"] = res
		this.ServeJSON()
	}()
	var m struct {
		Id int
	}
	json.Unmarshal(this.Ctx.Input.RequestBody, &m)
	logs.Info(m.Id)
	f,h,err := this.GetFile("file")
	utils.PanicErr(err)
	defer f.Close()

	if h.Filename != "data.in" && h.Filename != "data.out" {
		res["msg"] = h.Filename+" is not data.in OR data.out"
		return
	}
	dir := temp_dir+ fmt.Sprint(this.GetSession("user").(models.User).Id) + string(os.PathSeparator)
	err = os.MkdirAll(dir,os.ModePerm)
	utils.PanicErr(err)
	err = this.SaveToFile("file", dir + h.Filename)
	utils.PanicErr(err)
	finfo, err := os.Stat(dir + h.Filename)
	utils.PanicErr(err)
	res[h.Filename] = fmt.Sprint(finfo.ModTime())
	res["success"] = true
}

func init() {
	beego.Router(beego.AppConfig.String("rootpath") + "/api/upload_judge_data", &JudgeData{})
	beego.InsertFilter(beego.AppConfig.String("rootpath") + "/api/upload_judge_data", beego.BeforeExec, filters.CheckLogin)
	temp_dir = beego.AppConfig.String("temp_root_dir") + string(os.PathSeparator)
	err := os.MkdirAll(temp_dir, os.ModePerm)
	utils.PanicErr(err)
}