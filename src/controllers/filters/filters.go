package filters

import (
	"github.com/astaxie/beego/context"
	"../../models"
	"encoding/json"
)


func CheckLogin(ctx *context.Context) {
	if ctx.Input.Session("user") == nil {
		ctx.Output.SetStatus(200)
		var res = make(map[string]interface{})
		res["needlogin"] = true
		ctx.Output.JSON(res,false,false)
	}
}

func CheckContestAuth(ctx *context.Context) {
	if ctx.Input.Session("user") == nil {
		ctx.Output.SetStatus(200)
		var res = make(map[string]interface{})
		res["needlogin"] = true
		ctx.Output.JSON(res,false,false)
		return
	}
	userId := ctx.Input.Session("user").(models.User).Id
	var m struct {
		Contest_id int
	}
	json.Unmarshal(ctx.Input.RequestBody, &m)
	if !CheckContestAuthByContestID(userId, m.Contest_id) {
		var res = make(map[string]interface{})
		res["needauth"] = true
		ctx.Output.JSON(res,false,false)
	}
}

func CheckContestAuthByContestID(userId, contestId int) bool {
	if contestId <= 0 {
		panic("'contest_id' not found")
	}
	rows, _ := models.DB.Query(`select * from contest_participants where user_id = ? AND contest_id = ?`, userId, contestId)
	defer rows.Close()
	if !rows.Next() {
		return false
	}
	return true;
}