package problems

import (
	"github.com/astaxie/beego"
	"encoding/json"
	"../../models"
	"../../utils"
	remote_common "../../remote/common"
	"github.com/astaxie/beego/logs"
)

type ProblemDetail struct {
	beego.Controller
}

func (this *ProblemDetail) Post() {
	var res = make(map[string]interface{})
	defer func() {
		err := recover()
		if err!=nil {
			logs.Warn(err)
			res["success"] = false
			res["msg"] = "System Error"
		}
		this.Data["json"] = res
		this.ServeJSON()
	}()
	var m struct {
		Id int
	}
	json.Unmarshal(this.Ctx.Input.RequestBody, &m)

	rows, err := models.DB.Query(`select oj, remote_problem_id, title, time_limit, mem_limit, description, input, sample_input, output, sample_output, hint,author, public from problems_view where id=?`, m.Id)
	utils.PanicErr(err)
	defer rows.Close()

	if rows.Next() {
		var oj, remote_problem_id, title, time_limit, mem_limit, description, input, sample_input, output, sample_output, hint string
		var public bool
		var author int
		err = rows.Scan(&oj, &remote_problem_id, &title, &time_limit, &mem_limit, &description, &input, &sample_input, &output, &sample_output, &hint,&author, &public)
		utils.PanicErr(err)
		if !public {
			if this.GetSession("user")==nil || this.GetSession("user").(models.User).Id != author {
				res["success"] = false
				res["msg"] = "No such problem !"
				return
			}
		}
		res = map[string]interface{}{
			"success": true,
			"title": title,
			"time_limit": time_limit,
			"mem_limit": mem_limit,
			"description": description,
			"input": input,
			"sample_input": sample_input,
			"output": output,
			"sample_output": sample_output,
			"hint": hint,
			"public": public,
			"oj": oj,
			"remote_problem_id": remote_problem_id,
			"language_options": remote_common.LanguageOptions[oj],
		}
	} else {
		res["success"] = false
		res["msg"] = "No such problem !"
	}
}

func init() {
	beego.Router(beego.AppConfig.String("rootpath") + "/api/problem_detail", &ProblemDetail{})
}