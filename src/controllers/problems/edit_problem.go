package problems

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"encoding/json"
	"../../models"
	"../filters"
	"../../utils"
	"fmt"
	"os"
)

type EditProblem struct {
	beego.Controller
}


func (this *EditProblem) Post() {
	var res = make(map[string]interface{})
	defer func() {
		err := recover()
		if err!=nil {
			logs.Warn(err)
			res["success"] = false
			res["msg"] = "System Error"
		}
		this.Data["json"] = res
		this.ServeJSON()
	}()
	var m struct {
		Id int64
		Action string
		Title string
		Mem_limit int
		Time_limit int
		Description string
		Input string
		Output string
		Sample_input string
		Sample_output string
		Hint string
		DataInTime string
		DataOutTime string
		Public bool
	}
	json.Unmarshal(this.Ctx.Input.RequestBody, &m)
	if m.Title == "" {
		res["msg"] = "Title 不能为空"
		return
	}
	if m.Time_limit > 10 {
		res["msg"] = "TimeLimit 不能超过10 S"
		return
	}
	if m.Mem_limit > 512 {
		res["msg"] = "MemLimit 不能超过512 M"
		return
	}
	if m.Description == "" {
		res["msg"] = "Description 不能为空"
		return
	}
	var p models.Problem
	p.Oj = "fineoj"
	p.Id = m.Id
	p.RemoteProblemId = fmt.Sprint(m.Id)
	p.Title = m.Title
	p.MemLimit = fmt.Sprintf("%dM",m.Mem_limit)
	p.TimeLimit = fmt.Sprintf("%ds",m.Time_limit)
	p.Public = m.Public
	p.Description = m.Description
	p.Input = m.Input
	p.Output = m.Output
	p.SampleInput = m.Sample_input
	p.SampleOutput = m.Sample_output
	p.Hint = m.Hint
	p.AuthorId = this.GetSession("user").(models.User).Id

	dir := temp_dir+ fmt.Sprint(this.GetSession("user").(models.User).Id) + string(os.PathSeparator) //temp目录
	switch m.Action {
	case "create":
		p.RemoteProblemId = ""
		if m.DataInTime == "" || m.DataOutTime == "" {
			res["msg"] = "Please upload data.in and data.out first"
			return
		}
		datain_info, err := os.Stat(dir + "data.in")
		utils.PanicErr(err)
		dataout_info, err := os.Stat(dir + "data.out")
		utils.PanicErr(err)
		if m.DataInTime != fmt.Sprint(datain_info.ModTime()) || m.DataOutTime != fmt.Sprint(dataout_info.ModTime()) {
			res["msg"] = "data.in and data.out have been expired, please re-upload them."
			return
		}
		p.StoreFineOjProblem()
		dir2 := beego.AppConfig.String("judge_data_root_dir") + string(os.PathSeparator) + fmt.Sprint(p.Id) + string(os.PathSeparator) //production目录
		os.MkdirAll(dir2, os.ModePerm)
		err = os.Rename(dir+"data.in", dir2+"data.in")
		utils.PanicErr(err)
		err = os.Rename(dir+"data.out", dir2+"data.out")
		utils.PanicErr(err)
		break
	case "edit":
		if m.Id <=0 {
			res["msg"] = "Id <= 0"
			return
		}
		if m.DataInTime != "" {
			datain_info, err := os.Stat(dir + "data.in")
			utils.PanicErr(err)
			if m.DataInTime != fmt.Sprint(datain_info.ModTime()) {
				res["msg"] = "data.in and data.out have been expired, please re-upload them."
				return
			}
		}
		if m.DataOutTime != "" {
			dataout_info, err := os.Stat(dir + "data.out")
			utils.PanicErr(err)
			if m.DataOutTime != fmt.Sprint(dataout_info.ModTime()) {
				res["msg"] = "data.in and data.out have been expired, please re-upload them."
				return
			}
		}
		p.StoreFineOjProblem()
		dir2 := beego.AppConfig.String("judge_data_root_dir") + string(os.PathSeparator) + fmt.Sprint(p.Id) + string(os.PathSeparator) //production目录
		os.MkdirAll(dir2, os.ModePerm)
		if m.DataInTime != "" {
			err := os.Rename(dir+"data.in", dir2+"data.in")
			utils.PanicErr(err)
		}
		if m.DataOutTime != "" {
			err := os.Rename(dir+"data.out", dir2+"data.out")
			utils.PanicErr(err)
		}
		break
	default:
		res["msg"] = "unknown action!"
		return
	}
	res["success"] = true
}

var temp_dir string

func init() {
	beego.Router(beego.AppConfig.String("rootpath") + "/api/edit_problem", &EditProblem{})
	beego.InsertFilter(beego.AppConfig.String("rootpath") + "/api/edit_problem", beego.BeforeExec, filters.CheckLogin)
	temp_dir = beego.AppConfig.String("temp_root_dir") + string(os.PathSeparator)
	err := os.MkdirAll(temp_dir, os.ModePerm)
	utils.PanicErr(err)
}