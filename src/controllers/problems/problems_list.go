package problems

import (
	"github.com/astaxie/beego"
	"encoding/json"
	"../../models"
	"database/sql"
)

type ProblemsListController struct {
	beego.Controller
}

func (this *ProblemsListController) Post() {
	var res = make(map[string]interface{})
	var rows *sql.Rows
	defer func() {
		err := recover()
		if err!=nil {
			panic(err)
		} else {
			this.Data["json"] = res
			this.ServeJSON()
		}
	}()
	var m struct {
		Page int
		Offset int
		Limit int
		Oj string
		Search string
	}
	json.Unmarshal(this.Ctx.Input.RequestBody, &m)
	if m.Limit > 100 {
		m.Limit = 100
	}
	m.Search = "%" + m.Search + "%"
	var total int
	uid := 0
	if this.GetSession("user") != nil {
		uid = this.GetSession("user").(models.User).Id
	}
	if m.Oj == "all" {
		rows, _ = models.DB.Query(`select count(*) from problems where (public=1 OR author=?) AND (title like ? OR remote_problem_id like ?)`, uid, m.Search, m.Search)
	} else {
		rows, _ = models.DB.Query(`select count(*) from problems where oj=? AND (public=1 OR author=?) AND (title like ? OR remote_problem_id like ?)`,m.Oj,uid, m.Search, m.Search)
	}
	defer rows.Close()
	rows.Next()
	rows.Scan(&total)
	res["total"] = total

	type problemT struct {
		Id int
		Oj string
		RemoteProblemId string
		Title string
		UpdateTime string
		Author int
		Public bool
	}

	if m.Oj=="all" {
		rows, _ = models.DB.Query(`select id,oj,remote_problem_id,title,update_time,author,public from problems where (public=1 OR author=?) AND (title like ? OR remote_problem_id like ?) order by id desc limit ?,?`,uid, m.Search, m.Search, m.Offset, m.Limit)
	} else {
		rows, _ = models.DB.Query(`select id,oj,remote_problem_id,title,update_time,author,public from problems where oj=? AND (public=1 OR author=?) AND (title like ? OR remote_problem_id like ?) order by id desc limit ?,?`, m.Oj,uid, m.Search, m.Search, m.Offset, m.Limit)
	}
	defer rows.Close()

	var problems []problemT
	for rows.Next() {
		var row problemT
		rows.Scan(&row.Id, &row.Oj, &row.RemoteProblemId, &row.Title, &row.UpdateTime, &row.Author, &row.Public)
		problems = append(problems, row)
	}
	res["problems"] = problems
}

func init() {
	beego.Router(beego.AppConfig.String("rootpath") + "/api/problems_list", &ProblemsListController{})
}