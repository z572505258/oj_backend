package users

import (
	"github.com/astaxie/beego"
	"../../models"
	"../../utils"
	"encoding/json"
)

type LoginController struct {
	beego.Controller
}
type CheckLoginController struct {
	beego.Controller
}
type LogoutController struct {
	beego.Controller
}

func (this *LogoutController) Post() {
	var res = make(map[string]interface{})
	defer func() {
		this.Data["json"] = res
		this.ServeJSON()
	}()
	this.DelSession("user")
	if this.GetSession("user") != nil {
		res["success"] = false
	} else {
		res["success"] = true
	}
}

func (this *CheckLoginController) Post() {
	var res = make(map[string]interface{})
	defer func() {
		this.Data["json"] = res
		this.ServeJSON()
	}()
	res["user"] = this.GetSession("user")
}

func (this *LoginController) Post() {
	var res = make(map[string]interface{})
	defer func() {
		err := recover()
		if err!=nil {
			panic(err)
		} else {
			this.Data["json"] = res
			this.ServeJSON()
		}
	}()
	var m struct {
		Username string
		Password string
	}
	json.Unmarshal(this.Ctx.Input.RequestBody, &m)

	rows, err := models.DB.Query(`select id from users where username = ? AND password=?`, m.Username, m.Password)
	utils.PanicErr(err)
	defer rows.Close()

	if !rows.Next() {
		res["success"] = false
		res["msg"] = "用户名或密码错误"
		return
	}
	var id int
	err = rows.Scan(&id)
	utils.PanicErr(err)
	var user models.User
	user.Id = id
	user.Username = m.Username
	this.SetSession("user", user)
	res["success"] = true
	res["user"] = user
}




func init() {
	beego.Router(beego.AppConfig.String("rootpath") + "/api/login", &LoginController{})
	beego.Router(beego.AppConfig.String("rootpath") + "/api/checkLogin", &CheckLoginController{})
	beego.Router(beego.AppConfig.String("rootpath") + "/api/logout", &LogoutController{})
}