package users

import (
	"github.com/astaxie/beego"
	"../../models"
	"../../utils"
	"encoding/json"
)

type RegisterController struct {
	beego.Controller
}

func (this *RegisterController) Post() {
	var res = make(map[string]interface{})
	defer func() {
		err := recover()
		if err != nil {
			panic(err)
		} else {
			this.Data["json"] = res
			this.ServeJSON()
		}
	}()
	var m struct {
		Username string
		Password string
	}
	json.Unmarshal(this.Ctx.Input.RequestBody, &m)
	if len(m.Username) < 3 || len(m.Username) > 20 {
		panic("username length must 3 ~ 20")
	}

	stmt, err := models.DB.Prepare(`select count(*) as sum from users where username = ?`)
	defer stmt.Close()
	utils.PanicErr(err)

	rows, err := stmt.Query(m.Username)
	defer rows.Close()
	utils.PanicErr(err)

	rows.Next()
	var sum int
	rows.Scan(&sum)
	if sum != 0 {
		res["success"] = false
		res["msg"] = "用户名已存在"
		return
	}
	stmt, _ = models.DB.Prepare(`INSERT users (username, password) values (?,?)`)
	defer stmt.Close()
	_, err = stmt.Exec(m.Username, m.Password)
	if err != nil {
		res["success"] = false
		res["msg"] = "数据格式不符合要求"
	} else {
		res["success"] = true
	}
}

func init() {
	beego.Router(beego.AppConfig.String("rootpath")+"/api/register", &RegisterController{})
}
