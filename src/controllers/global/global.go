package global

import (
	"github.com/astaxie/beego"
	remote_common "../../remote/common"
)

type Global struct {
	beego.Controller
}

func (this *Global) Post() {
	this.Data["json"] = remote_common.GetOjsOption()
	this.ServeJSON()
}

func init() {
	beego.Router(beego.AppConfig.String("rootpath")+"/api/global", &Global{})
}
