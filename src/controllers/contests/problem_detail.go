package contests

import (
	"github.com/astaxie/beego"
	"encoding/json"
	"../../models"
	"../filters"
	"../../utils"
	remote_common "../../remote/common"
)

type ProblemDetail struct {
	beego.Controller
}

func (this *ProblemDetail) Post() {
	var res = make(map[string]interface{})
	defer func() {
		err := recover()
		if err != nil {
			panic(err)
		} else {
			this.Data["json"] = res
			this.ServeJSON()
		}
	}()

	var m struct {
		Contest_id int64
		Problem_index int64
	}
	json.Unmarshal(this.Ctx.Input.RequestBody, &m)

	rows, _ := models.DB.Query(`
		SELECT
			contest_problems.contest_id,
			contest_problems.problem_index,
			problems_view.oj,
			problems_view.title,
			problems_view.time_limit,
			problems_view.mem_limit,
			problems_view.description,
			problems_view.input,
			problems_view.sample_input,
			problems_view.output,
			problems_view.sample_output,
			problems_view.hint
		FROM contest_problems left join problems_view on contest_problems.problem_id = problems_view.id 
		where contest_problems.contest_id = ? and contest_problems.problem_index = ?
	`, m.Contest_id, m.Problem_index)
	defer rows.Close()
	if rows.Next() {
		var oj, title, time_limit, mem_limit, description, input, sample_input, output, sample_output, hint string
		var contest_id, problem_index int64
		err := rows.Scan(&contest_id, &problem_index, &oj, &title, &time_limit, &mem_limit, &description, &input, &sample_input, &output, &sample_output, &hint)
		utils.PanicErr(err)
		res["success"] = true
		res["contest_id"] = contest_id
		res["problem_index"] = problem_index
		res["title"] = title
		res["time_limit"] = time_limit
		res["mem_limit"] = mem_limit
		res["description"] = description
		res["input"] = input
		res["sample_input"] = sample_input
		res["output"] = output
		res["sample_output"] = sample_output
		res["hint"] = hint
		res["language_options"] = remote_common.LanguageOptions[oj]
	} else {
		res["success"] = false
		res["msg"] = "No such problem !"
	}
}

func init() {
	beego.Router(beego.AppConfig.String("rootpath") + "/api/contest_problem_detail", &ProblemDetail{})
	beego.InsertFilter(beego.AppConfig.String("rootpath") + "/api/contest_problem_detail", beego.BeforeExec, filters.CheckContestAuth)
}
