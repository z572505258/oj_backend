package contests

import (
	"github.com/astaxie/beego"
	"encoding/json"
	"../../models"
	"database/sql"
)

type ContestsListController struct {
	beego.Controller
}

func (this *ContestsListController) Post() {
	var res = make(map[string]interface{})
	var rows *sql.Rows
	defer func() {
		err := recover()
		if err != nil {
			panic(err)
		} else {
			this.Data["json"] = res
			this.ServeJSON()
		}
	}()
	var m struct {
		Page   int
		Offset int
		Limit  int
		Search string
	}
	json.Unmarshal(this.Ctx.Input.RequestBody, &m)
	if m.Limit > 100 {
		m.Limit = 100
	}
	m.Search = "%" + m.Search + "%"
	var total int
	//uid := 0
	//if this.GetSession("user") != nil {
	//	uid = this.GetSession("user").(models.User).Id
	//}
	rows, _ = models.DB.Query(`select count(*) from contests where title like ?`, m.Search)
	defer rows.Close()
	rows.Next()
	rows.Scan(&total)
	res["total"] = total

	type contestT struct {
		Id              int
		Type            int
		Title           string
		CreateTime      string
		Author          int
	}

	rows, _ = models.DB.Query(`select id,type,title,author,create_time from contests where title like ? order by id desc limit ?,?`, m.Search, m.Offset, m.Limit)
	defer rows.Close()

	var contests []contestT
	for rows.Next() {
		var row contestT
		rows.Scan(&row.Id, &row.Type, &row.Title, &row.Author, &row.CreateTime)
		contests = append(contests, row)
	}
	res["contests"] = contests
}

func init() {
	beego.Router(beego.AppConfig.String("rootpath") + "/api/contests_list", &ContestsListController{})
}
