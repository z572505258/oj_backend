package contests

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"encoding/json"
	"../../models"
	"../filters"
	"database/sql"
)

type EditContest struct {
	beego.Controller
}

func (this *EditContest) Post() {
	var res = make(map[string]interface{})
	defer func() {
		err := recover()
		if err != nil {
			logs.Warn(err)
			res["success"] = false
			res["msg"] = "System Error"
		}
		this.Data["json"] = res
		this.ServeJSON()
	}()
	var m struct {
		Id         int64
		Action     string
		Title      string
		Type       int64
		Time_range []string
		Password   string
		Problems []struct {
			Problem_id int64
			Index      int64
		}
	}
	json.Unmarshal(this.Ctx.Input.RequestBody, &m)
	if m.Title == "" {
		res["msg"] = "标题不能为空"
		return
	}
	if len(m.Time_range) != 2 {
		res["msg"] = "请选择比赛时间"
		return
	}
	if m.Type == 1 && len(m.Password) < 4 {
		res["msg"] = "密码长度至少 4 位"
		return
	}
	var c models.Contest
	c.Id = m.Id
	c.Title = m.Title
	c.Type = m.Type
	c.AuthorId = this.GetSession("user").(models.User).Id
	c.StartTime = m.Time_range[0]
	c.EndTime = m.Time_range[1]
	c.Password = m.Password
	for i := 0; i < len(m.Problems); i++ {
		c.Problems = append(c.Problems, models.ContestProblem{
			ProblemId : m.Problems[i].Problem_id,
			ProblemIndex : m.Problems[i].Index,
		})
	}

	switch m.Action {
	case "create":
		c.StoreContest()
		break
	case "edit":
		//if m.Id <=0 {
		//	res["msg"] = "Id <= 0"
		//	return
		//}
		//if m.DataInTime != "" {
		//	datain_info, err := os.Stat(dir + "data.in")
		//	utils.PanicErr(err)
		//	if m.DataInTime != fmt.Sprint(datain_info.ModTime()) {
		//		res["msg"] = "data.in and data.out have been expired, please re-upload them."
		//		return
		//	}
		//}
		//if m.DataOutTime != "" {
		//	dataout_info, err := os.Stat(dir + "data.out")
		//	utils.PanicErr(err)
		//	if m.DataOutTime != fmt.Sprint(dataout_info.ModTime()) {
		//		res["msg"] = "data.in and data.out have been expired, please re-upload them."
		//		return
		//	}
		//}
		//p.StoreFineOjContest()
		//dir2 := beego.AppConfig.String("judge_data_root_dir") + string(os.PathSeparator) + fmt.Sprint(p.Id) + string(os.PathSeparator) //production目录
		//os.MkdirAll(dir2, os.ModePerm)
		//if m.DataInTime != "" {
		//	err := os.Rename(dir+"data.in", dir2+"data.in")
		//	utils.PanicErr(err)
		//}
		//if m.DataOutTime != "" {
		//	err := os.Rename(dir+"data.out", dir2+"data.out")
		//	utils.PanicErr(err)
		//}
		//break
	default:
		res["msg"] = "unknown action!"
		return
	}
	res["success"] = true
}

type ContestEditProblemSearch struct {
	beego.Controller
}

func (this *ContestEditProblemSearch) Post() {
	var res = make(map[string]interface{})
	var rows *sql.Rows
	defer func() {
		err := recover()
		if err != nil {
			panic(err)
		} else {
			this.Data["json"] = res
			this.ServeJSON()
		}
	}()
	var m struct {
		Oj     string
		Search string
	}
	json.Unmarshal(this.Ctx.Input.RequestBody, &m)
	uid := 0
	if this.GetSession("user") != nil {
		uid = this.GetSession("user").(models.User).Id
	}
	type problemT struct {
		Id              int
		Oj              string
		RemoteProblemId string
		Title           string
		UpdateTime      string
		Author          int
		Public          bool
	}
	var problems []problemT
	var sum = 0

	//先进行精确搜索
	if m.Oj == "all" {
		rows, _ = models.DB.Query(`select id,oj,remote_problem_id,title,update_time,author,public from problems where (public=1 OR author=?) AND (title = ? OR remote_problem_id = ?) order by id desc limit 100`, uid, m.Search, m.Search)
	} else {
		rows, _ = models.DB.Query(`select id,oj,remote_problem_id,title,update_time,author,public from problems where oj=? AND (public=1 OR author=?) AND (title = ? OR remote_problem_id = ?) order by id desc limit 100`, m.Oj, uid, m.Search, m.Search)
	}
	defer rows.Close()
	for rows.Next() {
		var row problemT
		rows.Scan(&row.Id, &row.Oj, &row.RemoteProblemId, &row.Title, &row.UpdateTime, &row.Author, &row.Public)
		problems = append(problems, row)
		sum++
	}
	rawSearch := m.Search
	m.Search = "%" + m.Search + "%"

	if sum < 100 {
		if m.Oj == "all" {
			rows, _ = models.DB.Query(
				`select id,oj,remote_problem_id,title,update_time,author,public from problems where (public=1 OR author=?) AND title != ? AND remote_problem_id != ? AND  (title like ? OR remote_problem_id like ?) order by id desc limit ?`,
				uid, rawSearch, rawSearch, m.Search, m.Search, 100-sum)
		} else {
			rows, _ = models.DB.Query(
				`select id,oj,remote_problem_id,title,update_time,author,public from problems where oj=? AND (public=1 OR author=?) AND title != ? AND remote_problem_id != ? AND (title like ? OR remote_problem_id like ?) order by id desc limit ?`,
				m.Oj, uid, rawSearch, rawSearch, m.Search, m.Search, 100-sum)
		}
		defer rows.Close()

		for rows.Next() {
			var row problemT
			rows.Scan(&row.Id, &row.Oj, &row.RemoteProblemId, &row.Title, &row.UpdateTime, &row.Author, &row.Public)
			problems = append(problems, row)
		}
	}
	res["problems"] = problems
}

func init() {
	beego.Router(beego.AppConfig.String("rootpath") + "/api/contest_edit_problem_search", &ContestEditProblemSearch{})


	beego.Router(beego.AppConfig.String("rootpath") + "/api/edit_contest", &EditContest{})
	beego.InsertFilter(beego.AppConfig.String("rootpath") + "/api/edit_contest", beego.BeforeExec, filters.CheckLogin)
}
