package contests

import (
	"github.com/astaxie/beego"
	"encoding/json"
	"../../models"
	"../filters"
)

type ContestDetail struct {
	beego.Controller
}

func (this *ContestDetail) Post() {
	var res = make(map[string]interface{})
	defer func() {
		err := recover()
		if err != nil {
			panic(err)
		} else {
			this.Data["json"] = res
			this.ServeJSON()
		}
	}()

	var m struct {
		Contest_id int64
	}
	json.Unmarshal(this.Ctx.Input.RequestBody, &m)

	rows, _ := models.DB.Query(`select id,author,title,start_time,end_time from contests where id = ?`, m.Contest_id)
	defer rows.Close()
	var contest struct {
		Id int64
		Author int64
		Title string
		Start_time string
		End_time string
	}
	if !rows.Next() {
		panic("contest not exist")
	}
	rows.Scan(&contest.Id, &contest.Author, &contest.Title, &contest.Start_time, &contest.End_time)
	res["id"] = contest.Id
	res["author"] = contest.Author
	res["title"] = contest.Title
	res["start_time"] = contest.Start_time
	res["end_time"] = contest.End_time
	type problemT struct {
		Index int64
		Title string
	}
	problems := make([]problemT, 0)

	rows, _ = models.DB.Query(`select contest_problems.problem_index, problems.title from contest_problems left join problems on contest_problems.problem_id = problems.id where contest_id = ?`, m.Contest_id)
	defer rows.Close()

	for rows.Next() {
		var problem problemT
		rows.Scan(&problem.Index, &problem.Title)
		problems = append(problems, problem)
	}
	res["problems"] = problems
	res["success"] = true
}

func init() {
	beego.Router(beego.AppConfig.String("rootpath") + "/api/contest_detail", &ContestDetail{})
	beego.InsertFilter(beego.AppConfig.String("rootpath") + "/api/contest_detail", beego.BeforeExec, filters.CheckContestAuth)
}
