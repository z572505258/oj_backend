package contests

import (
	"github.com/astaxie/beego"
	"../filters"
	"../../models"
	"github.com/astaxie/beego/logs"
	"../../utils"
	"encoding/json"
)

type EnterContest struct {
	beego.Controller
}

func (this *EnterContest) Post() {
	var res = make(map[string]interface{})
	defer func() {
		err := recover()
		if err!=nil {
			logs.Warn(err)
			res["success"] = false
			res["msg"] = "System Error"
		}
		this.Data["json"] = res
		this.ServeJSON()
	}()
	var m struct {
		Contest_id int64
		Password string
	}
	json.Unmarshal(this.Ctx.Input.RequestBody, &m)
	userId := this.GetSession("user").(models.User).Id
	//step 1 : 检查比赛是否开始
	rows, _ := models.DB.Query(`select * from contests where id = ? AND start_time <= CURRENT_TIMESTAMP`, m.Contest_id)
	defer rows.Close()
	if !rows.Next() {
		res["success"] = false
		res["msg"] = "比赛尚未开始"
		return
	}
	//step 2 : 检查该user是否已经参加该比赛
	rows, _ = models.DB.Query(`select * from contest_participants where user_id = ? AND contest_id = ?`, userId, m.Contest_id)
	defer rows.Close()
	if rows.Next() {
		res["success"] = true
		return
	}
	//step 3 : 检查是否需要密码
	rows, _ = models.DB.Query(`select * from contests where id = ? AND (type = 0 OR password = ?)`, m.Contest_id, m.Password)
	defer rows.Close()
	if rows.Next() {
		_, err := models.DB.Exec("INSERT INTO contest_participants (user_id, contest_id) VALUES (?,?)", userId, m.Contest_id)
		utils.PanicErr(err)
		res["success"] = true
		logs.Info("注册比赛成功")
		return
	} else {
		res["needpassword"] = true
		return
	}
}

func init() {
	beego.Router(beego.AppConfig.String("rootpath") + "/api/enter_contest", &EnterContest{})
	beego.InsertFilter(beego.AppConfig.String("rootpath") + "/api/enter_contest", beego.BeforeExec, filters.CheckLogin)
}