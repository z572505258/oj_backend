package contests

import (
	"github.com/astaxie/beego"
	"encoding/json"
	"../../models"
	"../filters"
)

type ContestZipSubmissions struct {
	beego.Controller
}

func (this *ContestZipSubmissions) Post() {
	var res = make(map[string]interface{})
	defer func() {
		err := recover()
		if err != nil {
			panic(err)
		} else {
			this.Data["json"] = res
			this.ServeJSON()
		}
	}()

	var m struct {
		Contest_id int64
	}
	json.Unmarshal(this.Ctx.Input.RequestBody, &m)

	participants := make(map[int64]string)
	rows, _ := models.DB.Query(`SELECT contest_participants.user_id,users.username FROM contest_participants left join users on contest_participants.user_id = users.id where contest_participants.contest_id = ?`, m.Contest_id)
	defer rows.Close()

	for rows.Next() {
		var userId int64
		var userName string
		rows.Scan(&userId, &userName)
		participants[userId] = userName
	}

	submissions := make([][4]int64, 0)

	rows, _ = models.DB.Query(`select user_id,status_code,contest_problem_index,submit_time from solutions where contest_id = ?`, m.Contest_id)
	defer rows.Close()

	for rows.Next() {
		var v1, v2, v3, v4 int64
		rows.Scan(&v1, &v2, &v3, &v4)
		submissions = append(submissions, [4]int64{v1, v2, v3, v4})
	}
	//tmp := 50000
	//for tmp > 0 {
	//	submissions = append(submissions, [3]int64{1, 3, 1523115603000})
	//	tmp--
	//}

	res["submissions"] = submissions
	res["participants"] = participants
	res["success"] = true
}

func init() {
	beego.Router(beego.AppConfig.String("rootpath")+"/api/contest_zip_submissions", &ContestZipSubmissions{})
	beego.InsertFilter(beego.AppConfig.String("rootpath")+"/api/contest_zip_submissions", beego.BeforeExec, filters.CheckContestAuth)
}
