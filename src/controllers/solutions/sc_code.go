package solutions

import (
	"github.com/astaxie/beego"
	"encoding/json"
	"../../models"
)

type SCCodeController struct {
	beego.Controller
}

func (this *SCCodeController) Post() {
	var res = make(map[string]interface{})
	defer func() {
		err := recover()
		if err!=nil {
			panic(err)
		} else {
			this.Data["json"] = res
			this.ServeJSON()
		}
	}()
	var m struct {
		SolutionId int
	}
	json.Unmarshal(this.Ctx.Input.RequestBody, &m)
	if this.GetSession("user") == nil {
		res["success"] = false;
		return
	}

	username := this.GetSession("user").(models.User).Username

	rows, _ := models.DB.Query("select id from solutions where id=? and username=?",m.SolutionId,username)
	defer rows.Close()
	if !rows.Next() {
		res["success"] = false;
		return
	}

	rows, _ = models.DB.Query("select code from solutions_detail where solution_id=?",m.SolutionId)
	defer rows.Close()
	rows.Next()
	var sc_code string
	rows.Scan(&sc_code)
	res["success"] = true
	res["sc_code"] = sc_code
}

func init() {
	beego.Router(beego.AppConfig.String("rootpath") + "/api/sc_code", &SCCodeController{})
}