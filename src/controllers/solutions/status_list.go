package solutions

import (
	"github.com/astaxie/beego"
	"encoding/json"
	"../../models"
	"database/sql"
	"../filters"
)

type StatusListController struct {
	beego.Controller
}

func (this *StatusListController) Post() {
	var res = make(map[string]interface{})
	var rows *sql.Rows
	defer func() {
		err := recover()
		if err != nil {
			panic(err)
		} else {
			this.Data["json"] = res
			this.ServeJSON()
		}
	}()
	var m struct {
		Page       int
		Offset     int
		Limit      int
		Username   string
		ShowMyself bool
		ProblemId  int
		Contest_id int
	}
	m.ProblemId = 0
	m.Username = "" //????
	json.Unmarshal(this.Ctx.Input.RequestBody, &m)
	if m.Contest_id > 0 {
		if this.GetSession("user") == nil || !filters.CheckContestAuthByContestID(this.GetSession("user").(models.User).Id, m.Contest_id) {
			res["total"] = 0
			return
		}
	} else {
		m.Contest_id = 0
	}

	if m.Limit > 100 {
		m.Limit = 100
	}
	if m.ShowMyself {
		if this.GetSession("user") != nil {
			m.Username = this.GetSession("user").(models.User).Username
		} else {
			res["total"] = 0
			return
		}
	}
	var total int

	if m.Username != "" {
		if m.ProblemId > 0 {
			rows, _ = models.DB.Query(`select count(*) from solutions where contest_id=? and username=? and problem_id=?`, m.Contest_id, m.Username, m.ProblemId)
		} else {
			rows, _ = models.DB.Query(`select count(*) from solutions where contest_id=? and username=?`, m.Contest_id, m.Username)
		}
	} else {
		if m.ProblemId > 0 {
			rows, _ = models.DB.Query(`select count(*) from solutions where contest_id=? and problem_id=?`, m.Contest_id, m.ProblemId)
		} else {
			rows, _ = models.DB.Query(`select count(*) from solutions where contest_id=?`, m.Contest_id)
		}
	}
	defer rows.Close()
	rows.Next()
	rows.Scan(&total)
	res["total"] = total

	type statusT struct {
		Id         int
		ProblemId  string
		Status     string
		StatusCode int
		ExeTime    string
		ExeMem     string
		Language   string
		CodeLength string
		Username   string
		SubmitTime int64
		ContestProblemIndex int
	}

	if m.Username != "" {
		if m.ProblemId > 0 {
			rows, _ = models.DB.Query("select id,problem_id,status,status_code,exe_time,exe_mem,`language`,code_length,username,submit_time,contest_problem_index from solutions where contest_id=? and username=? and problem_id=? order by id desc limit ?,?", m.Contest_id, m.Username, m.ProblemId, m.Offset, m.Limit)
		} else {
			rows, _ = models.DB.Query("select id,problem_id,status,status_code,exe_time,exe_mem,`language`,code_length,username,submit_time,contest_problem_index from solutions where contest_id=? and username=? order by id desc limit ?,?", m.Contest_id, m.Username, m.Offset, m.Limit)
		}
	} else {
		if m.ProblemId > 0 {
			rows, _ = models.DB.Query("select id,problem_id,status,status_code,exe_time,exe_mem,`language`,code_length,username,submit_time,contest_problem_index from solutions where contest_id=? and problem_id=? order by id desc limit ?,?", m.Contest_id, m.ProblemId, m.Offset, m.Limit)
		} else {
			rows, _ = models.DB.Query("select id,problem_id,status,status_code,exe_time,exe_mem,`language`,code_length,username,submit_time,contest_problem_index from solutions where contest_id=? order by id desc limit ?,?", m.Contest_id, m.Offset, m.Limit)
		}
	}
	defer rows.Close()

	var status []statusT
	for rows.Next() {
		var row statusT
		rows.Scan(&row.Id, &row.ProblemId, &row.Status, &row.StatusCode, &row.ExeTime, &row.ExeMem, &row.Language, &row.CodeLength, &row.Username, &row.SubmitTime, &row.ContestProblemIndex)
		if m.Contest_id > 0 {
			row.ProblemId = ""
		}
		status = append(status, row)
	}
	res["status"] = status
}

func init() {
	beego.Router(beego.AppConfig.String("rootpath") + "/api/status_list", &StatusListController{})
}
