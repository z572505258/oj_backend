package solutions

import (
	"github.com/astaxie/beego"
	"github.com/gorilla/websocket"
	"../../remote"
	"../../utils"
	"../../models"
	"../filters"
	"database/sql"
)

type SubmitCode struct {
	beego.Controller
}

func (this *SubmitCode) Get() {
	var res = make(map[string]interface{})
	defer func() {
		err := recover()
		if err != nil {
			panic(err)
		}
		this.ServeJSON()
	}()
	var upgrader = websocket.Upgrader{} // ???????????
	ws, err := upgrader.Upgrade(this.Ctx.ResponseWriter, this.Ctx.Request, nil)
	utils.PanicErr(err)
	defer ws.Close()
	var m struct {
		LocalPid     int
		Code         string
		LanguageCode string

		Contest_id    int
		Problem_index int
	}
	err = ws.ReadJSON(&m)
	utils.PanicErr(err)
	if this.GetSession("user") == nil {
		res["ErrorMessage"] = "needlogin"
		ws.WriteJSON(res)
		return
	}
	userId := this.GetSession("user").(models.User).Id

	if (m.Contest_id > 0 && m.LocalPid > 0) || (m.Contest_id <= 0 && m.LocalPid <= 0) {
		res["ErrorMessage"] = "wrong pam"
		ws.WriteJSON(res)
		return
	}

	var rows *sql.Rows

	if m.Contest_id > 0 {
		if !filters.CheckContestAuthByContestID(userId, m.Contest_id) { //判断是否注册比赛
			res["ErrorMessage"] = "needauth"
			ws.WriteJSON(res)
			return
		}
		//获取题目id
		rows, _ = models.DB.Query(`select problem_id from contest_problems where contest_id = ? and problem_index = ?`, m.Contest_id, m.Problem_index)
		defer rows.Close()
		if !rows.Next() {
			res["ErrorMessage"] = "No such problem"
			ws.WriteJSON(res)
			return
		}
		err := rows.Scan(&m.LocalPid)
		utils.PanicErr(err)
		rows, err = models.DB.Query("select id from problems where id=?", m.LocalPid)
		utils.PanicErr(err)
	} else {
		m.Contest_id = 0
		rows, err = models.DB.Query("select id from problems where id=? and (public=1 OR author=?)", m.LocalPid, userId)
	}

	defer rows.Close()
	if !rows.Next() {
		res["ErrorMessage"] = "No such problem"
		ws.WriteJSON(res)
		return
	}
	task := new(remote.Task) //GC
	task.Code = m.Code
	task.LocalPid = m.LocalPid

	task.ContestId = m.Contest_id
	task.ContestProblemIndex = m.Problem_index

	task.LanguageCode = m.LanguageCode
	task.SubmitUser = this.GetSession("user").(models.User)
	task.JudgingStatus = make(chan remote.JudgingStatus, 100)
	remote.Tasks <- task
	for {
		step := <-task.JudgingStatus
		ws.WriteJSON(step)
		if remote.IsComplete(step.StatusCode) {
			break
		}
	}
}

func init() {
	beego.Router(beego.AppConfig.String("rootpath") + "/api/ws/submit_code", &SubmitCode{})
}
