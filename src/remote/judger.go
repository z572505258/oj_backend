package remote

import (
	"../models"
	"../utils"
	"time"
	"github.com/astaxie/beego/logs"
	"os"
	"./common"
	_ "./ojs"
	"./manager"
	"sync"
)

func IsComplete(StatusCode int) bool {
	return StatusCode < 20
}

type JudgingStatus struct {
	Status            string
	StatusCode        int
	AdditionalMessage string //用于传输CE信息等
}

type Task struct {
	LocalPid            int
	LanguageCode        string
	Code                string
	SubmitUser          models.User
	ContestId           int
	ContestProblemIndex int
	JudgingStatus       chan JudgingStatus //用于实时返回状态
}

var _ojs map[string]chan common.Oj

var Tasks chan *Task

func judge(task *Task) {
	//获取oj类型
	rows, err := models.DB.Query("select oj, remote_problem_id from problems where id=?", task.LocalPid)
	if err != nil {
		logs.Warn("step=[judger.judge()] info=[Data Base Query Error] task.LocalPid=[%d]", task.LocalPid)
		task.JudgingStatus <- JudgingStatus{
			StatusCode: common.STATUS_SystemError,
			Status:     "Database Error",
		}
		return
	}
	defer rows.Close()
	if !rows.Next() {
		logs.Warn("step=[judger.judge()] info=[No Such Problem In Database] task.LocalPid=[%d]", task.LocalPid)
		task.JudgingStatus <- JudgingStatus{
			StatusCode: common.STATUS_SystemError,
			Status:     "No Such Problem",
		}
		return
	}
	var oj_t, pid string
	err = rows.Scan(&oj_t, &pid)
	if err != nil {
		logs.Warn("step=[judger.judge()] info=[Rows Scan Error] task.LocalPid=[%d]", task.LocalPid)
		task.JudgingStatus <- JudgingStatus{
			StatusCode: common.STATUS_SystemError,
			Status:     "Database error",
		}
		return
	}
	//在插入solution之前，先检查oj是否存在
	if _ojs[oj_t] == nil {
		logs.Warn("step=[judger.judge()] info=[Unknown OJ type '%s'] task.LocalPid=[%d]", oj_t, task.LocalPid)
		//_, err = models.DB.Exec("update solutions set status='Unknown OJ',status_code=? where id=?", common.STATUS_SystemError, SolutionId)
		//if err != nil {
		//	logs.Warn(err)
		//}
		task.JudgingStatus <- JudgingStatus{
			StatusCode: common.STATUS_SystemError,
			Status:     "Unknown OJ type",
		}
		return
	}

	//插入solution
	res, err := models.DB.Exec("INSERT INTO solutions (problem_id,status,status_code,user_id,username,language,code_length,contest_id,contest_problem_index,submit_time) VALUES (?,?,?,?,?,?,?,?,?,?)", task.LocalPid, "Waiting", common.STATUS_Waiting, task.SubmitUser.Id, task.SubmitUser.Username, common.LanguageOptions[oj_t][task.LanguageCode].Label, len(task.Code), task.ContestId, task.ContestProblemIndex, models.GetNowTimestamp())
	if err != nil {
		logs.Warn(err)
		task.JudgingStatus <- JudgingStatus{
			StatusCode: common.STATUS_SystemError,
			Status:     "Database Error",
		}
		return
	}
	SolutionId, err := res.LastInsertId()
	if err != nil {
		logs.Warn(err)
		task.JudgingStatus <- JudgingStatus{
			StatusCode: common.STATUS_SystemError,
			Status:     "Database Error",
		}
		return
	}
	_, err = models.DB.Exec("INSERT INTO solutions_detail (solution_id,code) VALUES (?,?)", SolutionId, task.Code)
	if err != nil {
		logs.Warn(err)
		task.JudgingStatus <- JudgingStatus{
			StatusCode: common.STATUS_SystemError,
			Status:     "Database Error",
		}
		return
	}
	logs.Debug("start judge task.LocalPid=[%d]", task.LocalPid)

	oj := <-_ojs[oj_t]
	oj.SetIsBusy(true)
	defer func() {
		logs.Debug("%s step=[judger.judge() judge end] step=[defer func]", oj.GetLogInfo())
		err := recover()
		if err != nil {
			logs.Warn("%s step=[judger.judge()] panic=[%s] task.LocalPid=[%d]", oj.GetLogInfo(), err, task.LocalPid)
			_, err = models.DB.Exec("update solutions set status='Submit Failed',status_code=? where id=?", common.STATUS_SubmitFailed, SolutionId)
			if err != nil {
				logs.Warn(err)
			}
			oj.SetPreRunId("null") //按理来说提交失败之后这个值应该不变，但是需要"考虑可能实际上提交了，但却返回提交失败"的情况
			task.JudgingStatus <- JudgingStatus{
				StatusCode: common.STATUS_SubmitFailed,
				Status:     "Submit Failed",
			}
		}
		_ojs[oj_t] <- oj
		oj.SetIsBusy(false)
	}()
	//获取到OJ之后开始提交
	logs.Debug("%s step=[judger.judge() judge start] preRunId=[%s]", oj.GetLogInfo(), oj.GetPreRunId())
	if oj.GetPreRunId() == "null" { //上一次提交失败了，囧
		logs.Warn("%s step=[judger.judge()] info=[oj.GetPreRunId==\"null\"] action=[GetStatus] task.LocalPid=[%d]", oj.GetLogInfo(), task.LocalPid)
		oj.SetPreRunId(oj.GetStatus().RunId)
	} else if oj.GetPreRunId() == "" {
		logs.Error("%s step=[judger.judge()] info=[oj.GetPreRunId==\"\"] task.LocalPid=[%d]", oj.GetLogInfo(), task.LocalPid)
		os.Exit(2)
	}
	preRunId := oj.GetPreRunId()
	_, err = models.DB.Exec("update solutions set status='Submitting',status_code=? where id=?", common.STATUS_Submitting, SolutionId)
	if err != nil {
		logs.Warn(err)
	}
	if oj_t != "fineoj" { //remoteoj
		task.JudgingStatus <- JudgingStatus{
			StatusCode: common.STATUS_Submitting,
			Status:     "Submitting",
		}
		oj.Submit(pid, task.Code, task.LanguageCode)
		task.JudgingStatus <- JudgingStatus{
			StatusCode: common.STATUS_Submitted,
			Status:     "Submitted",
		}
		time.Sleep(1 * time.Second)
	} else {
		//这里的pid和task.LocalPid必须是相等的
		oj.Submit(pid, task.Code, task.LanguageCode) //开始编译
	}
	status := oj.GetStatus()
	preRunStatus := status.RunStatus
	nowRunId := "NULL=="
	if preRunId == status.RunId {
		logs.Warn("%s info=[submit failed] preRunId=[%s] nowRunId=[%s]", oj.GetLogInfo(), preRunId, status.RunId)
		panic("preRunId==nowRunId")
	} else {
		nowRunId = status.RunId
		logs.Info("%s info=[submitted] nowRunId=[%s] preRunId=[%s]", oj.GetLogInfo(), nowRunId, preRunId)
		oj.SetPreRunId(status.RunId)
		_, err = models.DB.Exec("update solutions set status='Submitted',status_code=? where id=?", common.STATUS_Submitted, SolutionId)
		if err != nil {
			logs.Warn(err)
		}
		_, err = models.DB.Exec("update solutions_detail set remote_runid=? where solution_id=?", status.RunId, SolutionId)
		if err != nil {
			logs.Warn(err)
		}
		if !IsComplete(status.StatusCode) {
			task.JudgingStatus <- JudgingStatus{
				StatusCode: status.StatusCode,
				Status:     status.RunStatus,
			}
		}
	}
	for !IsComplete(status.StatusCode) {
		if status.RunId != nowRunId {
			logs.Warn("%s info=[nowRunId!=status.RunId]")
			panic("nowRunId==status.RunId")
		}
		if status.RunStatus != preRunStatus {
			_, err = models.DB.Exec("update solutions set status=?,status_code=? where id=?", status.RunStatus, status.StatusCode, SolutionId)
			if err != nil {
				logs.Warn(err)
			}
			task.JudgingStatus <- JudgingStatus{
				StatusCode: status.StatusCode,
				Status:     status.RunStatus,
			}
			preRunStatus = status.RunStatus
		}
		if oj_t != "fineoj" {
			time.Sleep(50 * time.Millisecond)
		}
		status = oj.GetStatus()
	}
	logs.Debug("%s preRunId=[%s] nowRunId=[%s] ret=[%s] exe_time=[%s] exe_mem=[%s]", oj.GetLogInfo(), preRunId, status.RunId, status.RunStatus, status.ExeTime, status.ExeMem)
	_, err = models.DB.Exec("update solutions set status=?,status_code=?,exe_time=?,exe_mem=? where id=?", status.RunStatus, status.StatusCode, status.ExeTime, status.ExeMem, SolutionId)
	if err != nil {
		logs.Warn(err)
	}
	AdditionalMessage := ""
	if status.StatusCode == common.STATUS_CompilationError {
		AdditionalMessage = status.CompilationErrorInfo
		_, err = models.DB.Exec("update solutions_detail set CE_info=? where solution_id=?", status.CompilationErrorInfo, SolutionId)
		if err != nil {
			logs.Warn(err)
		}
	}
	task.JudgingStatus <- JudgingStatus{
		StatusCode:        status.StatusCode,
		Status:            status.RunStatus,
		AdditionalMessage: AdditionalMessage,
	}
}

func run() {
	for {
		task := <-Tasks //读取任务
		go judge(task)
	}
}

func init() {
	_ojs = make(map[string]chan common.Oj)
	ojs_mu := sync.RWMutex{}
	ojs_sum := make(map[string]*counter)
	ojs_sum_mu := sync.RWMutex{}
	rows, err := models.DB.Query("select oj, username, password from oj_accounts")
	utils.PanicErr(err)
	defer rows.Close()
	for rows.Next() {
		var oj_t, username, password string
		err = rows.Scan(&oj_t, &username, &password)
		utils.PanicErr(err)
		ojs_mu.Lock()
		if _ojs[oj_t] == nil {
			_ojs[oj_t] = make(chan common.Oj, 32) //每个oj最多有32个账号
			ojs_mu.Unlock()
			ojs_sum_mu.Lock()
			ojs_sum[oj_t] = &counter{} //改变map结构
			ojs_sum_mu.Unlock()
			ojs_sum[oj_t].setValue(32)
		} else {
			ojs_mu.Unlock()
		}
		var _oj common.Oj
		_oj = common.GetOjInstance(oj_t)
		if _oj == nil {
			logs.Warn("%s not registered", oj_t)
			continue
		}
		logs.Info("%s judger account +1 %s", oj_t, _oj.GetLogInfo())

		go func(__oj common.Oj, _username string, _password string) {
			ojs_sum_mu.RLock()
			if ojs_sum[oj_t].getCnt() > 0 {
				__oj.Init(username, password)
				ojs_sum[oj_t].decrease() //
				ojs_sum_mu.RUnlock()
				ojs_mu.RLock()     //这里是读锁，因为并没有改变map结构
				_ojs[oj_t] <- __oj //login成功可能在这之后
				ojs_mu.RUnlock()
				ojs_manager.InsertOj(oj_t, __oj)
				logs.Info("%s info=[initialized] step=[added into _ojs]", __oj.GetLogInfo())
			} else {
				ojs_sum_mu.RUnlock()
				logs.Warn("%s ojs_sum > 32 Please Delete Some accounts from database")
			}
		}(_oj, username, password)
	}
	Tasks = make(chan *Task, 8) //最多同时只能有 n 道题在judge
	go run()
}
