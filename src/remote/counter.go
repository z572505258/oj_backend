package remote

import "sync"

type counter struct {
	cnt int
	mu sync.RWMutex
}
func (this *counter) setValue(v int) {
	this.mu.Lock()
	this.cnt = v
	this.mu.Unlock()
}
func (this *counter) decrease() {
	this.mu.Lock()
	this.cnt--
	this.mu.Unlock()
}
func (this *counter) getCnt() int {
	this.mu.RLock()
	defer this.mu.RUnlock()
	return this.cnt
}