package ojs

import (
	"../../utils"
	_ "net/url"
	"net/url"
	"regexp"
	"fmt"
	"../common"
	"golang.org/x/text/encoding/simplifiedchinese"
	"io/ioutil"
	"github.com/astaxie/beego/logs"
	"../../models"
	"strconv"
	"golang.org/x/text/transform"
)

/*
OJ Judger 编码说明：init 函数不能panic，即使登录失败，也要继续
 */

type Hdu struct {
	common.Ojbase
}

func (this *Hdu) Init(usernmae string, password string) {
	this.Ojbase.LogInfo = fmt.Sprintf("oj=[HDU] username=[%s]", usernmae)
	this.Ojbase.CkCookiePage = "http://acm.hdu.edu.cn" //包含username的页面
	this.Ojbase.Login = this.Login                     //这里感觉比较奇怪，我试来试去只能这么写，ojbase的成员函数不能访问hdu的Login，所以只能用闭包。
	this.Ojbase.Init(usernmae, password)
	defer func() {
		err := recover()
		if err != nil { //GetStatus产生panic
			this.PreRunId = "null"
		}
	}()
	this.PreRunId = this.GetStatus().RunId //注意PreRunId是线程不安全的，所以init函数是线程不安全的
}

func (this *Hdu) Login() {
	loginurl := "http://acm.hdu.edu.cn/userloginex.php?action=login"
	postForm := url.Values{}
	postForm.Set("username", this.Username)
	postForm.Set("userpass", this.Password)
	this.Client.Get("http://acm.hdu.edu.cn")
	_, err := this.Client.PostForm(loginurl, postForm)
	utils.PanicErr(err)
}

func (this *Hdu) GetStatus() common.Status {
	html := utils.ClientGetGBK(this.Client, "http://acm.hdu.edu.cn/status.php?user="+this.Username)
	var ret common.Status
	ret.RunId = utils.MatchSubLeftMost(html, `<td height=22px>([0-9].*?)</td>`)
	tr := regexp.MustCompile(`<tr align=center[\s\S]*?` + ret.RunId + `[\s\S]*?</tr>`).FindString(html)
	tds := utils.ProcessTr(tr)
	ret.RunId = tds[0]
	ret.RunStatus = tds[2]
	ret.ExeTime = tds[4]
	ret.ExeMem = tds[5]
	if b, _ := regexp.MatchString("Runtime Error", ret.RunStatus); b {
		ret.StatusCode = common.STATUS_RuntimeError
	} else {
		switch ret.RunStatus {
		case "Queuing":
			ret.StatusCode = common.STATUS_Queuing
			break
		case "Compiling":
			ret.StatusCode = common.STATUS_Compiling
			break
		case "Running":
			ret.StatusCode = common.STATUS_Running
			break
		case "Accepted":
			ret.StatusCode = common.STATUS_Accept
			break
		case "Wrong Answer":
			ret.StatusCode = common.STATUS_WrongAnswer
			break
		case "Presentation Error":
			ret.StatusCode = common.STATUS_PresentaionError
			break
		case "Time Limit Exceeded":
			ret.StatusCode = common.STATUS_TimeLimitExec
			break
		case "Memory Limit Exceeded":
			ret.StatusCode = common.STATUS_MemLimitExec
			break
		case "Output Limit Exceeded":
			ret.StatusCode = common.STATUS_OutputLimitExec
			break
		case "Compilation Error":
			ret.StatusCode = common.STATUS_CompilationError
			_html := utils.ClientGetGBK(this.Client, "http://acm.hdu.edu.cn/viewerror.php?rid="+ret.RunId)
			ret.CompilationErrorInfo = utils.MatchSubLeftMost(_html, `<pre>([\s\S]*?)</pre>`)
			ret.CompilationErrorInfo = utils.HtmlToNormal(ret.CompilationErrorInfo)
			break
		default:
			ret.StatusCode = common.STATUS_UndefinedStatus
		}
	}
	return ret
}

func (this *Hdu) Submit(pid string, code string, languageCode string) {
	code += "\n\n\n//fine oj . any problems , please contact 572505258@qq.com"
	code, err := simplifiedchinese.GBK.NewEncoder().String(code)
	utils.PanicErr(err)
	posturl := "http://acm.hdu.edu.cn/submit.php?action=submit"
	postForm := url.Values{}
	postForm.Set("problemid", pid)
	postForm.Set("language", languageCode)
	postForm.Set("usercode", code)
	rsp, err := this.Client.PostForm(posturl, postForm)
	utils.PanicErr(err) //提交失败
	defer rsp.Body.Close()
}

func (this *Hdu) CrawlProblem(pidStr string) *models.Problem {
	pid, err := strconv.Atoi(pidStr)
	utils.PanicErr(err)
	host := "http://acm.hdu.edu.cn/"
	oj := "hdu"
	nowdir := "/"
	logs.Info("正在抓取hdu %d", pid)
	rsp, err := this.Client.Get(fmt.Sprintf(host+"showproblem.php?pid=%d", pid))
	utils.PanicErr(err)
	defer rsp.Body.Close()
	body, err := ioutil.ReadAll(transform.NewReader(rsp.Body, simplifiedchinese.GBK.NewDecoder()))
	utils.PanicErr(err)
	html := string(body[:])
	problem := new(models.Problem)
	//检查题目是否存在
	t, _ := regexp.MatchString("<TD><img src=\"/images/msg.png\"", html)
	if t {
		panic(common.CRAWL_PROBLEM_NOT_EXIST)
	}
	problem.Oj = "hdu"
	problem.RemoteProblemId = fmt.Sprintf("%d", pid)
	//Title
	problem.Title = utils.MatchSubLeftMost(html, `color:#1A5CC8'>([\s\S]*?)</h1>`)
	//TimeLimit
	problem.TimeLimit = utils.MatchSubLeftMost(html, `/(\d* MS)`)
	//MemLimit
	problem.MemLimit = utils.MatchSubLeftMost(html, `/(\d* K)`)
	problem.Description = utils.MatchSubLeftMostProPic(html, `<div class=panel_content>([\s\S]*?)</div><div class=panel_bottom>&nbsp;</div>`, host, nowdir, oj)
	problem.Input = utils.MatchSubLeftMostProPic(html, `<div class=panel_title align=left>Input</div> <div class=panel_content>([\s\S]*?)</div><div class=panel_bottom>&nbsp;</div>`, host, nowdir, oj)
	problem.Output = utils.MatchSubLeftMostProPic(html, `<div class=panel_title align=left>Output</div> <div class=panel_content>([\s\S]*?)</div><div class=panel_bottom>&nbsp;</div>`, host, nowdir, oj)
	problem.SampleInput = utils.MatchSubLeftMostProPic(html, `<div class=panel_title align=left>Sample Input</div><div class=panel_content><pre><div style="font-family:Courier New,Courier,monospace;">([\s\S]*?)</div></pre>`, host, nowdir, oj)
	problem.SampleOutput = utils.MatchSubLeftMostProPic(html, `<div class=panel_title align=left>Sample Output</div><div class=panel_content><pre><div style="font-family:Courier New,Courier,monospace;">([\s\S]*?)(<div style='font-family:Times New Roman;[\s\S]*?<i style='font-size:1px'> </i>[\s\S]*?)?</div></pre>`, host, nowdir, oj)
	if regexp.MustCompile(`<i style='font-size:1px'> </i></div></pre>`).MatchString(html) {
		problem.Hint = utils.MatchSubLeftMostProPic(html, `<i>Hint</i></div>([\s\S]*?)</div><i style='font-size:1px'> </i>`, host, nowdir, oj)
	} else {
		problem.Hint = utils.MatchSubLeftMostProPic(html, `<i style='font-size:1px'> </i>([\s\S]*?)</div></pre>`, host, nowdir, oj)
	}
	return problem
}

func (this *Hdu) GetNextProblem(pidStr string) string {
	if pidStr == "" {
		return "1000"
	}
	pid, err := strconv.Atoi(pidStr)
	utils.PanicErr(err)
	return fmt.Sprint(pid + 1)
}

func init() {
	common.LanguageOptions["hdu"] = make(map[string]common.LanguageOption)
	common.LanguageOptions["hdu"]["0"] = common.LanguageOption{Label:"G++", Editor:common.EDITOR_C_CPP}
	common.LanguageOptions["hdu"]["1"] = common.LanguageOption{Label:"GCC", Editor:common.EDITOR_C_CPP}
	common.LanguageOptions["hdu"]["2"] = common.LanguageOption{Label:"C++", Editor:common.EDITOR_C_CPP}
	common.LanguageOptions["hdu"]["3"] = common.LanguageOption{Label:"C", Editor:common.EDITOR_C_CPP}
	common.LanguageOptions["hdu"]["4"] = common.LanguageOption{Label:"Pascal", Editor:common.EDITOR_PASCAL}
	common.LanguageOptions["hdu"]["5"] = common.LanguageOption{Label:"Java", Editor:common.EDITOR_JAVA}
	common.LanguageOptions["hdu"]["6"] = common.LanguageOption{Label:"C#", Editor:common.EDITOR_CSHARP}
	common.RegisterOj("hdu","HDU", func() common.Oj {
		return new(Hdu)
	})
}
