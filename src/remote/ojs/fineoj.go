package ojs
import (
	"../common"
	"os"
	"fmt"
	core_utils "../../fineoj_core/utils"
	"../../utils"
	"github.com/astaxie/beego/logs"
	"syscall"
	"time"
	"strings"
	"../../models"
)
/*
OJ Judger 编码说明：init 函数不能panic，即使登录失败，也要继续
 */

type Fineoj struct {
	common.Ojbase
	fakeRunId int
	pid string
	code string
	languageCode string
	status int
	memLimit string
	timeLimit string
}

func (this *Fineoj)Init(usernmae string, password string) {
	this.Ojbase.IsLogin = true
	this.Ojbase.IsBusy = false
	this.Ojbase.LogInfo = "oj=[FineOJ] "
	this.PreRunId = "fake_runid_0"
	this.fakeRunId = 0
	this.Username = usernmae
}
func (this *Fineoj)GetStatus() common.Status { //阻塞函数
	var procAttr os.ProcAttr
	procAttr.Files = []*os.File{os.Stdin, os.Stdout, os.Stderr}
	run_dir := wd+string(os.PathSeparator)+"run"+string(os.PathSeparator)+startTime+string(os.PathSeparator)+this.Username+string(os.PathSeparator)+fmt.Sprintf("%d",this.fakeRunId)
	if this.status == 0 {
		this.status = common.STATUS_Compiling
		return common.Status{
			RunId: fmt.Sprintf("fake_runid_%d", this.fakeRunId),
			RunStatus: "Compiling",
			StatusCode: common.STATUS_Compiling,
		}
	} else if this.status == common.STATUS_Compiling {
		this.status = common.STATUS_Running
		src_file_name := "Main.cpp"
		os.MkdirAll(run_dir, os.ModePerm)
		src_file, err := os.Create(run_dir+string(os.PathSeparator)+src_file_name)
		if err != nil {
			logs.Warn(err)
			this.status = -1
			return common.Status{
				RunId: fmt.Sprintf("fake_runid_%d", this.fakeRunId),
				RunStatus: "System Error",
				StatusCode: common.STATUS_SystemError,
			}
		}
		src_file.Write([]byte(this.code))
		src_file.Close()

		args := make([]string,4)
		args[1] = run_dir
		args[2] = src_file_name
		args[3] = "c++"
		p, err := os.StartProcess("compiler.exe", args, &procAttr)
		if err !=nil {
			logs.Warn(err)
			this.status = -1
			return common.Status{
				RunId: fmt.Sprintf("fake_runid_%d", this.fakeRunId),
				RunStatus: "System Error",
				StatusCode: common.STATUS_SystemError,
			}
		}
		ps,err := p.Wait()
		if err != nil {
			logs.Warn(err)
			this.status = -1
			return common.Status{
				RunId: fmt.Sprintf("fake_runid_%d", this.fakeRunId),
				RunStatus: "System Error",
				StatusCode: common.STATUS_SystemError,
			}
		}
		if ps.Exited() {
			if !ps.Success() {
				logs.Debug("Compilation Error")
				this.status = -1
				f,err := os.Open(run_dir+"\\CEInfo.txt")
				if err != nil {
					logs.Warn("Cannot open CEInfo.txt", err)
					return common.Status{
						RunId: fmt.Sprintf("fake_runid_%d", this.fakeRunId),
						RunStatus: "System Error",
						StatusCode: common.STATUS_SystemError,
					}
				}
				defer f.Close()
				buf := make([]byte,1000)
				var tmp string
				for {
					n,err := f.Read(buf)
					if n==0 {
						break
					}
					if err != nil {
						logs.Warn("Read CEInfo.txt error",err)
						return common.Status{
							RunId: fmt.Sprintf("fake_runid_%d", this.fakeRunId),
							RunStatus: "System Error",
							StatusCode: common.STATUS_SystemError,
						}
					}
					tmp += string(buf[:n])
				}
				return common.Status{
					RunId: fmt.Sprintf("fake_runid_%d", this.fakeRunId),
					RunStatus: "Compilation Error",
					StatusCode: common.STATUS_CompilationError,
					CompilationErrorInfo: tmp,
				}
			}
		} else {
			logs.Error("!ps.Exited")
			this.status = -1
			return common.Status{
				RunId: fmt.Sprintf("fake_runid_%d", this.fakeRunId),
				RunStatus: "System Error",
				StatusCode: common.STATUS_SystemError,
			}
		}
		return common.Status{
			RunId: fmt.Sprintf("fake_runid_%d", this.fakeRunId),
			RunStatus: "Running",
			StatusCode: common.STATUS_Running,
		}
	} else if this.status == common.STATUS_Running {
		this.status = -1
		args := make([]string,7)
		args[1] = run_dir
		args[2] = "Main.exe"
		args[3] = wd+"\\tests\\test_case\\"+this.pid+"\\data.in" //data.in
		args[4] = wd+"\\tests\\test_case\\"+this.pid+"\\data.out" //data.out
		args[5] = fmt.Sprint(this.memLimit)
		args[6] = fmt.Sprint(this.timeLimit)
		//RUNNNNNNN
		p, err := os.StartProcess("executor.exe", args, &procAttr)
		if err !=nil {
			logs.Warn(err)
			return common.Status{
				RunId: fmt.Sprintf("fake_runid_%d", this.fakeRunId),
				RunStatus: "System Error",
				StatusCode: common.STATUS_SystemError,
			}
		}
		ps,err := p.Wait()
		if err != nil {
			logs.Warn(err)
			return common.Status{
				RunId: fmt.Sprintf("fake_runid_%d", this.fakeRunId),
				RunStatus: "System Error",
				StatusCode: common.STATUS_SystemError,
			}
		}
		if ps.Exited() {
			coreStatusCode := ps.Sys().(syscall.WaitStatus).ExitStatus()
			logs.Info(ps.SysUsage())
			logs.Debug(coreStatusCode)
			if coreStatus_to_Status(int(coreStatusCode)) == common.STATUS_SystemError {
				logs.Warn(coreStatusCode)
			}
			return common.Status {
				RunId: fmt.Sprintf("fake_runid_%d", this.fakeRunId),
				RunStatus: core_utils.GetStatus(coreStatusCode),
				StatusCode: coreStatus_to_Status(int(coreStatusCode)),
				//ExeMem:
			}
		} else {
			logs.Warn("!ps.Exited")
			return common.Status{
				RunId: fmt.Sprintf("fake_runid_%d", this.fakeRunId),
				RunStatus: "System Error",
				StatusCode: common.STATUS_SystemError,
			}
		}
	} else {
		logs.Warn("Unknown Error")
		return common.Status{
			RunId: fmt.Sprintf("fake_runid_%d", this.fakeRunId),
			RunStatus: "System Error",
			StatusCode: common.STATUS_SystemError,
		}
	}
}

func (this *Fineoj)Submit(pid, code, languageCode string) {
	this.fakeRunId++
	this.pid = pid
	this.code = code
	this.languageCode = languageCode
	this.status = 0
	rows, err := models.DB.Query("select mem_limit, time_limit from problems_detail where id = ?", pid)
	utils.PanicErr(err)
	defer rows.Close()
	if !rows.Next() {
		panic("can not find problems_detail")
	}
	err = rows.Scan(&this.memLimit, &this.timeLimit)
	utils.PanicErr(err)
}

func coreStatus_to_Status(coreStatus int) int {
	switch coreStatus {
	case core_utils.STATUS_ACCEPT:
		return common.STATUS_Accept
	case core_utils.STATUS_TIME_LIMIT_EXCEEDED:
		return common.STATUS_TimeLimitExec
	case core_utils.STATUS_MEMORY_LIMIT_EXCEEDED:
		return common.STATUS_MemLimitExec
	case core_utils.STATUS_RUNTIME_ERROR:
		return common.STATUS_RuntimeError
	default:
		return common.STATUS_SystemError
	}
}

var wd,startTime string

func init() {
	common.LanguageOptions["fineoj"] = make(map[string]common.LanguageOption)
	common.LanguageOptions["fineoj"]["0"] = common.LanguageOption{Label:"C++", Editor:common.EDITOR_C_CPP}
	wd_, err := os.Getwd()
	if err != nil {
		os.Exit(2)
	}
	wd = wd_
	startTime = time.Now().String()
	startTime = strings.Replace(startTime,":","_",-1)
	startTime = strings.Replace(startTime," ","_",-1)
	common.RegisterOj("fineoj","FineOJ", func() common.Oj {
		return new(Fineoj)
	})
}