package ojs

import (
	"../../utils"
	"io/ioutil"
	"net/url"
	"fmt"
	"../common"
	"github.com/astaxie/beego/logs"
	"time"
	"regexp"
	"strconv"
	"net/http"
	"../../models"
)

/*
OJ Judger 编码说明：init 函数不能panic，即使登录失败，也要继续
 */

type Cf struct {
	common.Ojbase
	csrf_token string
}

func (this *Cf) Init(usernmae string, password string) {
	this.Ojbase.LogInfo = fmt.Sprintf("oj=[CF] username=[%s]", usernmae)
	this.Ojbase.CkCookiePage = "http://codeforces.com/" //包含username的页面
	this.Ojbase.Login = this.Login
	this.Ojbase.Init(usernmae, password)
	defer func() {
		err := recover()
		if err != nil { //GetStatus产生panic
			this.PreRunId = "null"
			logs.Warn(err)
		}
	}()
	this.PreRunId = this.GetStatus().RunId //注意PreRunId是线程不安全的，所以init函数是线程不安全的
}

func (this *Cf) Login() {
	//logs.Debug("%s login start", this.LogInfo)
	loginurl := "http://codeforces.com/enter"
	rsp, err := this.Client.Get(loginurl)
	utils.PanicErr(err)
	defer rsp.Body.Close()
	body, err := ioutil.ReadAll(rsp.Body)
	utils.PanicErr(err)
	this.updateCSRF(string(body[:]))

	postForm := url.Values{}
	postForm.Set("csrf_token", this.csrf_token)
	postForm.Set("action", "enter")
	postForm.Set("ftaa", "")
	postForm.Set("bfaa", "")
	postForm.Set("handleOrEmail", this.Username)
	postForm.Set("password", this.Password)

	rsp, err = this.Client.PostForm(loginurl, postForm)
	defer rsp.Body.Close()
	body, err = ioutil.ReadAll(rsp.Body)
	utils.PanicErr(err)
	this.updateCSRF(string(body[:]))
}

func (this *Cf) GetStatus() common.Status {
	//logs.Debug("%s getting status", this.LogInfo)
	html := utils.ClientGet(this.Client, "http://codeforces.com/submissions/"+this.Username)

	this.updateCSRF(html)

	var ret common.Status
	ret.RunId = utils.MatchSubLeftMost(html, `<tr data-submission-id="([0-9]*?)"`)
	tr := regexp.MustCompile(`<tr data-submission-id="` + ret.RunId + `[\s\S]*?</tr>`).FindString(html)
	tds := utils.ProcessTr(tr)
	ret.RunStatus = tds[5]
	ret.ExeTime = utils.HtmlToNormal(tds[6])
	ret.ExeMem = utils.HtmlToNormal(tds[7])
	if regexp.MustCompile(`Compilation error`).MatchString(ret.RunStatus) {
		ret.StatusCode = common.STATUS_CompilationError
		posturl := "http://codeforces.com/data/judgeProtocol"
		postForm := url.Values{}
		postForm.Set("submissionId", ret.RunId)
		postForm.Set("csrf_token", this.csrf_token)
		CEInfo := utils.ClientPost(this.Client, postForm, posturl)
		CEInfo, err := strconv.Unquote(CEInfo)
		utils.PanicErr(err)
		ret.CompilationErrorInfo = CEInfo
	} else if (regexp.MustCompile(`Accepted`).MatchString(ret.RunStatus)) {
		ret.StatusCode = common.STATUS_Accept
	} else if (regexp.MustCompile(`Wrong answer on test`).MatchString(ret.RunStatus)) {
		ret.StatusCode = common.STATUS_WrongAnswer
	} else if (regexp.MustCompile(`Runtime`).MatchString(ret.RunStatus)) {
		ret.StatusCode = common.STATUS_RuntimeError
	} else if (regexp.MustCompile(`Time limit`).MatchString(ret.RunStatus)) {
		ret.StatusCode = common.STATUS_TimeLimitExec
	} else if (regexp.MustCompile(`Memory limit`).MatchString(ret.RunStatus)) {
		ret.StatusCode = common.STATUS_MemLimitExec
	} else if (regexp.MustCompile(`Running`).MatchString(ret.RunStatus)) {
		ret.StatusCode = common.STATUS_Running
	} else {
		ret.StatusCode = common.STATUS_UndefinedStatus
	}
	//logs.Debug(ret)
	return ret
}

func (this *Cf) updateCSRF(html string) {
	this.csrf_token = utils.MatchSubLeftMost(html, `<meta name="X-Csrf-Token" content="([\s\S]*?)"/>`)
	if len(this.csrf_token) < 5 {
		panic("csrf error")
	}
}

func (this *Cf) Submit(pid, code, languageCode string) {
	//logs.Debug("%s submitting", this.LogInfo)
	code += "\n\n\n//fine oj . any problems " + fmt.Sprint(time.Now())
	posturl := "http://codeforces.com/problemset/submit?csrf_token=" + this.csrf_token
	postForm := url.Values{}
	postForm.Set("csrf_token", this.csrf_token)
	postForm.Set("action", "submitSolutionFormSubmitted")
	postForm.Set("ftaa", "")
	postForm.Set("programTypeId", languageCode)
	postForm.Set("source", code)
	postForm.Set("submittedProblemCode", pid)
	postForm.Set("sourceCodeConfirmed", "true")
	postForm.Set("doNotShowWarningAgain", "on")
	rsp, err := this.Client.PostForm(posturl, postForm)
	utils.PanicErr(err) //提交失败
	defer rsp.Body.Close()
	body, err := ioutil.ReadAll(rsp.Body)
	utils.PanicErr(err)
	html := string(body[:])
	this.updateCSRF(html)
}

func (this *Cf) CrawlProblem(pidStr string) *models.Problem {
	cId, pId := cfStrToCidPid(pidStr)

	host := "http://codeforces.com/"
	oj := "cf"
	nowdir := fmt.Sprintf("/problemset/problem/%d/%c", cId, pId)
	logs.Info("正在抓取cf %d %c", cId, pId)
	rsp, err := http.Get(fmt.Sprintf(host+"problemset/problem/%d/%c", cId, pId)) //不能使用this.Client，否则会重定向到上一题
	utils.PanicErr(err)
	defer rsp.Body.Close()
	body, err := ioutil.ReadAll(rsp.Body)
	utils.PanicErr(err)
	html := string(body[:])
	problem := new(models.Problem)
	problem.Oj = "cf"
	problem.RemoteProblemId = fmt.Sprintf("%d%c", cId, pId)
	if regexp.MustCompile(`<title>Codeforces</title>`).MatchString(html) {
		panic(common.CRAWL_PROBLEM_NOT_EXIST)
	}
	//Title
	problem.Title = utils.MatchSubLeftMost(html, `<div class="title">[A-Z]\. ([\s\S]*?)</div><div class="time-limit">`)
	//TimeLimit
	problem.TimeLimit = utils.MatchSubLeftMost(html, `>time limit per test</div>([\s\S]*?)</div>`)
	//MemLimit
	problem.MemLimit = utils.MatchSubLeftMost(html, `>memory limit per test</div>([\s\S]*?)</div>`)
	inputFile := utils.MatchSubLeftMost(html, `<div class="property-title">input</div>([\s\S]*?)</div>`)
	inputFile = "Input File: <span>" + inputFile + "</span><br/>"
	outputFile := utils.MatchSubLeftMost(html, `<div class="property-title">output</div>([\s\S]*?)</div>`)
	outputFile = "Output File: <span>" + outputFile + "</span>"

	problem.Description = "<div class='cf-iofile'>" + inputFile + outputFile + "</div>" + utils.MatchSubProPic(html, `<div class="output-file"><div class="property-title">output</div>([\s\S]*?)</div></div><div>([\s\S]*?)</div><div class="input-specification">`, host, nowdir, oj, 2)
	problem.Input = utils.MatchSubLeftMostProPic(html, `<div class="section-title">Input</div>([\s\S]*?)</div><div class="output-specification">`, host, nowdir, oj)
	problem.Output = utils.MatchSubLeftMostProPic(html, `<div class="section-title">Output</div>([\s\S]*?)</div><div class="sample-tests">`, host, nowdir, oj)

	problem.SampleInput = "<style type=\"text/css\">.input, .output {border: 1px solid #888888;} .output {margin-bottom:1em;position:relative;top:-1px;} .output pre,.input pre {background-color:#EFEFEF;line-height:1.25em;margin:0;padding:0.25em;} .title {background-color:#FFFFFF;border-bottom: 1px solid #888888;font-family:arial;font-weight:bold;padding:0.25em;}</style>" + utils.MatchSubLeftMostProPic(html, `<div class="sample-test">([\s\S]*?)</div>\s*</div>\s*</div>`, host, nowdir, oj)

	problem.Hint = utils.MatchSubLeftMostProPic(html, `<div class="section-title">\s*Note\s*</div>([\s\S]*?)</div></div>`, host, nowdir, oj)
	return problem
}

func (this *Cf) GetNextProblem(pidStr string) string {
	if pidStr == "" {
		return "1A"
	}
	cId, pId := cfStrToCidPid(pidStr)
	if pId <= 'N' {
		pId++
	} else {
		cId++
		pId = 'A'
	}
	return fmt.Sprintf("%d%c", cId, pId)
}

func cfStrToCidPid(pidStr string) (int, uint8) {
	cId, err := strconv.Atoi(regexp.MustCompile(`^[0-9]*`).FindAllString(pidStr, 1)[0])
	utils.PanicErr(err)
	pId := pidStr[len(pidStr)-1]
	return cId, pId
}

func init() {
	common.LanguageOptions["cf"] = make(map[string]common.LanguageOption)
	common.LanguageOptions["cf"]["1"] = common.LanguageOption{Label: "G++", Editor: common.EDITOR_C_CPP}
	common.LanguageOptions["cf"]["42"] = common.LanguageOption{Label: "G++11", Editor: common.EDITOR_C_CPP}
	common.LanguageOptions["cf"]["50"] = common.LanguageOption{Label: "G++14", Editor: common.EDITOR_C_CPP}
	common.LanguageOptions["cf"]["53"] = common.LanguageOption{Label: "C++17", Editor: common.EDITOR_C_CPP}
	common.LanguageOptions["cf"]["32"] = common.LanguageOption{Label: "Go 1.8", Editor: common.EDITOR_GOLANG}
	common.LanguageOptions["cf"]["36"] = common.LanguageOption{Label: "Java 1.8.0_131", Editor: common.EDITOR_JAVA}
	common.LanguageOptions["cf"]["7"] = common.LanguageOption{Label: "Python 2.7", Editor: common.EDITOR_PYTHON}
	common.LanguageOptions["cf"]["31"] = common.LanguageOption{Label: "Python 3.6", Editor: common.EDITOR_PYTHON}
	common.LanguageOptions["cf"]["34"] = common.LanguageOption{Label: "JavaScript V8 4.8.0", Editor: common.EDITOR_JAVASCRIPT}
	common.RegisterOj("cf","CodeForces", func() common.Oj {
		return new(Cf)
	})
}
