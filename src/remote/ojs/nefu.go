package ojs

import (
	"../../utils"
	_ "net/url"
	"net/url"
	"regexp"
	"fmt"
	"../common"
	"io/ioutil"
	"github.com/astaxie/beego/logs"
	"../../models"
	"strconv"
	"crypto/md5"
	"io"
	"encoding/json"
	"net/http"
	"strings"
)

/*
OJ Judger 编码说明：init 函数不能panic，即使登录失败，也要继续
 */

type Nefu struct {
	common.Ojbase
}

func (this *Nefu) Init(usernmae string, password string) {
	this.Ojbase.LogInfo = fmt.Sprintf("oj=[NEFU] username=[%s]", usernmae)
	this.Ojbase.CkCookiePage = "http://acm.nefu.edu.cn/JudgeOnline/index.php" //包含username的页面
	this.Ojbase.Login = this.Login                                            //这里感觉比较奇怪，我试来试去只能这么写，ojbase的成员函数不能访问nefu的Login，所以只能用闭包。
	md5password := md5.New()
	io.WriteString(md5password, password)
	this.Ojbase.Init(usernmae, fmt.Sprintf("%x", md5password.Sum(nil)))
	defer func() {
		err := recover()
		if err != nil { //GetStatus产生panic
			this.PreRunId = "null"
		}
	}()
	this.PreRunId = this.GetStatus().RunId //注意PreRunId是线程不安全的，所以init函数是线程不安全的
}

func (this *Nefu) Login() {
	loginurl := "http://acm.nefu.edu.cn/JudgeOnline/post/user/loginPost.php?order=1"
	postForm := url.Values{}
	postForm.Set("userName", this.Username)
	postForm.Set("password", this.Password)
	postForm.Set("remember", "false")
	this.Client.Get("http://acm.nefu.edu.cn")

	req, err := http.NewRequest("POST", loginurl, strings.NewReader(postForm.Encode()))
	utils.PanicErr(err)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Origin", "http://acm.nefu.edu.cn")
	req.Header.Set("Referer", "http://acm.nefu.edu.cn/JudgeOnline/status.php")
	_, err = this.Client.Do(req)
	utils.PanicErr(err)
}

func (this *Nefu) GetStatus() common.Status {
	body := this.getBody("http://acm.nefu.edu.cn/JudgeOnline/post/status/statusListPost.php?limit=20&offset=0&pid=&uid="+this.Username+"&result=0&language=0&params%5Border%5D=asc&params%5Blimit%5D=20&params%5Boffset%5D=0", nil)

	var res map[string]interface{}
	json.Unmarshal(body, &res)
	row := res["rows"].([]interface{})[0].(map[string]interface{})

	var ret common.Status
	ret.RunId = row["solution_id"].(string)
	ret.ExeTime = row["time"].(string)
	ret.ExeMem = row["memory"].(string)
	ret.RunStatus = utils.ClearTags(row["result"].(string))
	if regexp.MustCompile(`Compile Error`).MatchString(ret.RunStatus) {
		ret.StatusCode = common.STATUS_CompilationError
		postForm := url.Values{}
		postForm.Set("solution_id", ret.RunId)
		body = this.getBody("http://acm.nefu.edu.cn/JudgeOnline/post/status/statusCEPost.php", postForm)

		var resCE map[string]interface{}
		json.Unmarshal(body, &resCE)
		CEInfo := resCE["data"].(map[string]interface{})["CEInformation"].(string)
		ret.CompilationErrorInfo = CEInfo
	} else if regexp.MustCompile(`Accepted`).MatchString(ret.RunStatus) {
		ret.StatusCode = common.STATUS_Accept
	} else if regexp.MustCompile(`Wrong Answer`).MatchString(ret.RunStatus) {
		ret.StatusCode = common.STATUS_WrongAnswer
	} else if regexp.MustCompile(`Runtime Error`).MatchString(ret.RunStatus) || regexp.MustCompile(`Floating Point Error`).MatchString(ret.RunStatus) {
		ret.StatusCode = common.STATUS_RuntimeError
	} else if regexp.MustCompile(`Time Limit Exceeded`).MatchString(ret.RunStatus) {
		ret.StatusCode = common.STATUS_TimeLimitExec
	} else if regexp.MustCompile(`Memory Limit Exceeded`).MatchString(ret.RunStatus) {
		ret.StatusCode = common.STATUS_MemLimitExec
	} else if regexp.MustCompile(`Presentation Error`).MatchString(ret.RunStatus) {
		ret.StatusCode = common.STATUS_PresentaionError
	} else if regexp.MustCompile(`Output Limit Exceeded`).MatchString(ret.RunStatus) {
		ret.StatusCode = common.STATUS_OutputLimitExec
	} else if regexp.MustCompile(`Running`).MatchString(ret.RunStatus) {
		ret.StatusCode = common.STATUS_Running
	} else {
		ret.StatusCode = common.STATUS_UndefinedStatus
	}
	logs.Debug(ret)
	return ret
}

func (this *Nefu) Submit(pid string, code string, languageCode string) {
	code += "\n\n\n//fineoj"
	postForm := url.Values{}
	postForm.Set("problem", pid)
	postForm.Set("language", languageCode)
	postForm.Set("code", code)
	this.getBody("http://acm.nefu.edu.cn/JudgeOnline/post/problem/submitWritePost.php", postForm)
}

func (this *Nefu) CrawlProblem(pidStr string) *models.Problem {
	postForm := url.Values{}
	postForm.Set("problem", pidStr)

	body := this.getBody("http://acm.nefu.edu.cn/JudgeOnline/post/problem/problemShowPost.php", postForm)

	var res map[string]interface{}
	json.Unmarshal(body, &res)

	if strings.Contains(res["message"].(string), "题目不存在") {
		panic(common.CRAWL_PROBLEM_NOT_EXIST)
	}

	problemJSON := res["data"].(map[string]interface{})

	if problemJSON["Deleted"]!=nil && problemJSON["Deleted"].(bool) {
		panic(common.CRAWL_PROBLEM_NOT_EXIST)
	}

	logs.Info("正在抓取nefu %s", pidStr)

	problem := new(models.Problem)
	problem.Oj = "nefu"
	problem.RemoteProblemId = pidStr
	problem.Title = problemJSON["Title"].(string)
	problem.Description = utils.ProcessPicture(problemJSON["Description"].(string), "http://acm.nefu.edu.cn", "JudgeOnline","nefu" )
	problem.SampleInput = utils.ProcessPicture(problemJSON["SampleInput"].(string), "http://acm.nefu.edu.cn", "JudgeOnline","nefu" )
	problem.SampleOutput = utils.ProcessPicture(problemJSON["SampleOutput"].(string), "http://acm.nefu.edu.cn", "JudgeOnline","nefu" )
	problem.MemLimit = problemJSON["MemoryLimit"].(string) + "K"
	problem.TimeLimit = problemJSON["TimeLimit"].(string) + "ms"
	problem.Input = problemJSON["Input"].(string)
	problem.Output = problemJSON["Output"].(string)
	problem.Hint = problemJSON["Hint"].(string)
	return problem
}

func (this *Nefu) GetNextProblem(pidStr string) string {
	if pidStr == "" {
		return "317"
	}
	pid, err := strconv.Atoi(pidStr)
	utils.PanicErr(err)
	return fmt.Sprint(pid + 1)
}

func (this *Nefu) getBody(_url string, postForm url.Values) []byte {
	req, err := http.NewRequest("POST", _url, strings.NewReader(postForm.Encode()))
	utils.PanicErr(err)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Origin", "http://acm.nefu.edu.cn")
	req.Header.Set("Referer", "http://acm.nefu.edu.cn/JudgeOnline/status.php")
	rsp, err := this.Client.Do(req)
	utils.PanicErr(err)
	body, err := ioutil.ReadAll(rsp.Body)
	utils.PanicErr(err)
	return body
}

func init() {
	common.LanguageOptions["nefu"] = make(map[string]common.LanguageOption)
	common.LanguageOptions["nefu"]["1"] = common.LanguageOption{Label:"G++ 5.4", Editor:common.EDITOR_C_CPP}
	common.LanguageOptions["nefu"]["2"] = common.LanguageOption{Label:"GCC 5.4", Editor:common.EDITOR_C_CPP}
	common.LanguageOptions["nefu"]["3"] = common.LanguageOption{Label:"JAVA", Editor:common.EDITOR_JAVA}
	common.RegisterOj("nefu","NEFU", func() common.Oj {
		return new(Nefu)
	})
}
