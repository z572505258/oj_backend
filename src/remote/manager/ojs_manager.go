package ojs_manager

/*
用于描述judgers的当前动态，用于实时反馈给用户
 */
import (
	"../common"
	"../../models"
	"sync"
	"time"
	"github.com/astaxie/beego/logs"
	"../../utils"
)

type ojInstance struct {
	OjInstance     common.Oj
	ExploreFailSum int
	ExploreNowPid	string
	FailSum         int
	NowPid         string
}

var ojInstances map[string][]*ojInstance
var ojInstancesMu sync.RWMutex

func InsertOj(ojType string, oj common.Oj) {
	maxPid := getMaxPidByOjType(ojType)
	ojInstancesMu.Lock()
	if ojInstances[ojType] == nil {
		ojInstances[ojType] = make([]*ojInstance, 0)
	}
	ojInstances[ojType] = append(ojInstances[ojType], &ojInstance{
		OjInstance: oj,
		ExploreFailSum: 0,
		FailSum: 0,
		NowPid: "",
		ExploreNowPid: maxPid})
	ojInstancesMu.Unlock()
}

func getMaxPidByOjType(ojType string) string {
	rows,err := models.DB.Query("select remote_problem_id from problems where oj=? order by id desc limit 1", ojType)
	utils.PanicErr(err)
	defer rows.Close()
	if rows.Next() {
		var pid string
		err = rows.Scan(&pid)
		utils.PanicErr(err)
		return pid
	} else {
		return ""
	}
}

type stat struct {
	OjType     string
	OnlineSum  int
	OfflineSum int
	BusySum    int
}

var ojsStat []*stat

func GetOjsStat() []*stat {
	return ojsStat
}

func crawl(o common.Oj, pid string) *models.Problem {
	defer func() {
		err := recover()
		if err != nil {
			logs.Warn(err)
		}
	}()
	p := o.CrawlProblem(pid)
	return p
}

func runCrawlUpdate() {
	for {
		//遍历所有ojType
		ojInstancesMu.RLock()
		for k, v := range ojInstances {
			if k == "fineoj" {
				continue
			}
			if len(v) == 0 {
				continue
			}
			if v[0].NowPid == "" {
				v[0].NowPid = v[0].OjInstance.GetNextProblem(v[0].NowPid)
			}
			p := crawl(v[0].OjInstance, v[0].NowPid)
			if p != nil {
				p.StoreRemoteProblem()
				v[0].FailSum = 0
			} else {
				logs.Warn("oj=[%s] msg=[crawl problem failed] pid=[%s]", k, v[0].NowPid)
				v[0].FailSum++
			}
			v[0].NowPid = v[0].OjInstance.GetNextProblem(v[0].NowPid)
			if v[0].FailSum >= 1000 {
				v[0].NowPid = v[0].OjInstance.GetNextProblem("")
				v[0].FailSum = 0
			}
		}
		ojInstancesMu.RUnlock()
	}
}

func runCrawlExplore() {
	for {
		//遍历所有ojType
		ojInstancesMu.RLock()
		for k, v := range ojInstances {
			if k == "fineoj" {
				continue
			}
			if len(v) == 0 {
				continue
			}
			if v[0].ExploreNowPid == "" {
				v[0].ExploreNowPid = v[0].OjInstance.GetNextProblem(v[0].ExploreNowPid)
			}
			p := crawl(v[0].OjInstance, v[0].ExploreNowPid)
			if p != nil {
				p.StoreRemoteProblem()
				v[0].ExploreFailSum = 0
			} else {
				logs.Warn("oj=[%s] msg=[crawl problem failed] pid=[%s]", k, v[0].ExploreNowPid)
				v[0].ExploreFailSum++
			}
			v[0].ExploreNowPid = v[0].OjInstance.GetNextProblem(v[0].ExploreNowPid)
			if v[0].ExploreFailSum >= 1000 {
				v[0].ExploreNowPid = getMaxPidByOjType(k)
				v[0].ExploreFailSum = 0
			}
		}
		ojInstancesMu.RUnlock()
	}
}

func runUpdStat() {
	busyTmpSum := 0
	onlineTmpSum := 0
	offlineTmpSum := 0
	ojsStat = make([]*stat, 0)
	statMap := make(map[string]*stat)
	for {
		ojInstancesMu.RLock()
		for k, v := range ojInstances {
			busyTmpSum = 0
			onlineTmpSum = 0
			offlineTmpSum = 0
			if statMap[k] == nil {
				statMap[k] = new(stat)
				statMap[k].BusySum = 0
				statMap[k].OfflineSum = 0
				statMap[k].OnlineSum = 0
				statMap[k].OjType = k
				ojsStat = append(ojsStat, statMap[k])
			}
			l := len(v)
			for i := 0; i < l; i++ {
				if v[i].OjInstance.GetIsLogin() {
					onlineTmpSum++
					if v[i].OjInstance.GetIsBusy() {
						busyTmpSum++
					}
				} else {
					offlineTmpSum++
				}
			}
			statMap[k].BusySum = busyTmpSum
			statMap[k].OfflineSum = offlineTmpSum
			statMap[k].OnlineSum = onlineTmpSum
		}
		ojInstancesMu.RUnlock()
		time.Sleep(time.Millisecond * 500)
	}
}

func init() {
	ojInstances = make(map[string][]*ojInstance)
	go runCrawlUpdate()
	go runCrawlExplore()
	go runUpdStat()
}
