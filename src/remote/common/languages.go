package common


var LanguageOptions map[string]map[string]LanguageOption

type LanguageOption struct {
	Label string
	Editor string
}

const (
	EDITOR_C_CPP = "c_cpp"
	EDITOR_JAVA = "java"
	EDITOR_CSHARP = "csharp"
	EDITOR_GOLANG = "golang"
	EDITOR_PYTHON = "python"
	EDITOR_RUBY = "ruby"
	EDITOR_RUST = "rust"
	EDITOR_PASCAL = "pascal"
	EDITOR_JAVASCRIPT = "javascript"
)




func init() {
	LanguageOptions = make(map[string]map[string]LanguageOption)
}