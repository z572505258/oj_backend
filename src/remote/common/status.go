package common

type Status struct {
	RunId string
	ExeTime string
	ExeMem string
	RunStatus string
	StatusCode int
	CompilationErrorInfo string
}

const (
	STATUS_SubmitFailed = 0
	STATUS_Accept = 1
	STATUS_WrongAnswer = 2
	STATUS_CompilationError = 3
	STATUS_RuntimeError = 4
	STATUS_TimeLimitExec = 5
	STATUS_MemLimitExec = 6
	STATUS_OutputLimitExec = 7
	STATUS_PresentaionError = 8
	STATUS_UndefinedError = 9
	STATUS_SystemError = 10

	STATUS_Waiting = 20
	STATUS_Submitting = 21
	STATUS_Submitted = 22
	STATUS_Queuing = 23
	STATUS_Compiling = 24
	STATUS_Running = 25
	STATUS_Judging = 26
	STATUS_UndefinedStatus = 27


	SYSERROR_WAIT4_ERROR = 91
	SYSERROR_CHILD_UNDEFINED_EXIT_CODE = 92



	CRAWL_PROBLEM_NOT_EXIST = "CRAWL_PROBLEM_NOT_EXIST"
	CRAWL_HTTP_ERROR = "CRAWL_HTTP_ERROR"
	CRAWL_BODY_READ_ERROR = "CRAWL_BODY_READ_ERROR"
	CRAWL_HTML_PARSE_ERROR = "CRAWL_HTML_PARSE_ERROR"
)