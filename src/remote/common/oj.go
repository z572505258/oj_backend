package common

import (
	"net/http"
	"golang.org/x/net/publicsuffix"
	"../../utils"
	"net/http/cookiejar"
	"github.com/astaxie/beego/logs"
	_ "fmt"
	"../../models"
	"io/ioutil"
	"golang.org/x/text/transform"
	"golang.org/x/text/encoding/simplifiedchinese"
	"regexp"
	"time"
	"os"
)

type Oj interface {
	Init(username string, password string)
	GetStatus() Status
	CkCookie()
	GetPreRunId() string
	SetPreRunId(runid string)
	Submit(pid string, code string, language string)
	GetLogInfo() string

	GetIsLogin() bool
	GetIsBusy() bool
	SetIsBusy(IsBusy bool)

	CrawlProblem(pid string) *models.Problem
	GetNextProblem(pid string) string
}

type Ojbase struct {
	Oj
	Username     string
	Password     string
	PreRunId     string
	IsLogin      bool
	IsBusy       bool
	Client       *http.Client
	LogInfo      string
	Login        func()
	CkCookiePage string
}

func (this *Ojbase) SetIsBusy(IsBusy bool) {
	this.IsBusy = IsBusy
}

func (this *Ojbase) GetIsBusy() bool {
	return this.IsBusy
}

func (this *Ojbase) GetIsLogin() bool {
	return this.IsLogin
}

func (this *Ojbase) Init(username string, password string) { //这里不会panic
	this.IsBusy = false
	this.IsLogin = false
	this.Username = username
	this.Password = password
	this.PreRunId = "null"

	jar, err := cookiejar.New(&cookiejar.Options{PublicSuffixList: publicsuffix.List})
	if err != nil {
		logs.Error("cookiejar.New Failed!")
		os.Exit(2)
	}

	this.Client = &http.Client{
		Jar:     jar,
		Timeout: time.Second * 10,
	}
	this.CkCookie() //这里不会panic，只登录一次，不做检查，如果失败，由下面的RunCkCookie重新登录
	go this.RunCkCookie()
}

func (this *Ojbase) CkCookie() {
	logs.Info("%s checking cookie", this.LogInfo)
	defer func() {
		err := recover()
		if err != nil {
			logs.Warn("%s step=[CkCookie] info=[login failed] panic=[%s] action=[switch IsLogin to false].", this.LogInfo, err)
			this.IsLogin = false
		}
	}()

	CheckIsLogin := func() bool {
		rsp, err := this.Client.Get(this.CkCookiePage)
		utils.PanicErr(err)
		defer rsp.Body.Close()
		body, err := ioutil.ReadAll(transform.NewReader(rsp.Body, simplifiedchinese.GBK.NewDecoder()))
		utils.PanicErr(err)
		ok, err := regexp.Match(this.Username, body)
		utils.PanicErr(err)
		return ok
	}

	if CheckIsLogin() {
		this.IsLogin = true
		return
	}
	this.Login() //这里不会影响submit函数，首先client是线程安全的，其次submit函数有完善的检测机制
	if CheckIsLogin() {
		logs.Info("%s step=[CkCookie] info=[login success] action=[switch IsLogin to true]", this.LogInfo)
		this.IsLogin = true
	} else {
		logs.Warn("%s step=[CkCookie] info=[login failed] action=[switch IsLogin to false]", this.LogInfo)
		this.IsLogin = false
	}
}

func (this *Ojbase) RunCkCookie() {
	retry := 0
	for {
		this.CkCookie()
		if this.IsLogin {
			time.Sleep(5 * time.Minute)
		} else {
			if retry < 5 {
				time.Sleep(2 * time.Second) //每隔2s重试一次
				retry++
			} else {
				time.Sleep(5 * time.Minute)
				retry = 0
			}
		}
	}
}

func (this *Ojbase) GetPreRunId() string {
	return this.PreRunId
}
func (this *Ojbase) GetLogInfo() string {
	return this.LogInfo
}

func (this *Ojbase) SetPreRunId(runid string) {
	this.PreRunId = runid
}

var getOjFuncs map[string]func() (Oj)

type ojOption struct {
	Value  string
	Label string
}

var ojsOptions []*ojOption

func RegisterOj(oj string, fullName string, getOjFunc func() (Oj)) {
	if getOjFuncs == nil {
		getOjFuncs = make(map[string]func() (Oj))
	}
	if ojsOptions == nil {
		ojsOptions = make([]*ojOption, 0)
	}
	ojsOptions = append(ojsOptions, &ojOption{Value: oj, Label: fullName})
	getOjFuncs[oj] = getOjFunc
}

func GetOjsOption() []*ojOption {
	return ojsOptions
}

func GetOjInstance(oj string) Oj {
	if getOjFuncs[oj] == nil {
		return nil
	}
	return getOjFuncs[oj]()
}
