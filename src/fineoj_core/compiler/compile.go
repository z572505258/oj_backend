package main

import (
	"os"
	"os/exec"
	"path/filepath"
	"io/ioutil"
)

func main() {
	SourceDir := os.Args[1]
	SourceFile := os.Args[2]
	os.Chdir(SourceDir)
	args := []string{
		"g++",
		SourceFile,
		"-o",
		"Main",
		"-fno-asm",
		"-Wall",
		"-lm",
		"-std=c++11",
		"-DONLINE_JUDGE",
	}
	cmd := &exec.Cmd{
		Path: args[0],
		Args: args,
	}
	if filepath.Base(args[0]) == args[0] {
		if lp, err := exec.LookPath(args[0]); err != nil {
			os.Exit(-1)
		} else {
			cmd.Path = lp
		}
	}
	stderr, err := cmd.StderrPipe()
	if err != nil {
		os.Exit(-2)
	}
	err = cmd.Start()
	if err != nil {
		os.Exit(-3)
	}
	error_, err := ioutil.ReadAll(stderr)
	if err != nil {
		os.Exit(-4)
	}
	err = cmd.Wait()
	if err != nil {
		error_file, err := os.Create("CEInfo.txt")
		if err != nil {
			os.Exit(2)
		}
		error_file.Write(error_)
		os.Exit(1) //编译错误
	}
	os.Exit(0)
}