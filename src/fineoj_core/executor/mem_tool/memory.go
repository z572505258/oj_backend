package mem_tool

import (
	"fmt"
	"syscall"
	"unsafe"
	"os"
	"../../utils"
)

const (
	READ_CONTROL                      = 0x00020000
	PROCESS_QUERY_INFORMATION         = 0x0400
	PROCESS_QUERY_LIMITED_INFORMATION = 0x1000
)

func LoadLib() {

}

type SyncedBufferT struct {
	buffer [256]byte
}

func CheckMemory(ch chan int, pid uintptr, memLimit int32) {
	Kernel, err := syscall.LoadLibrary("Kernel32.dll")
	if err != nil {
		panic(err)
	}
	defer syscall.FreeLibrary(Kernel)
	Psapi, err := syscall.LoadLibrary("Psapi.dll")
	if err != nil {
		panic(err)
	}
	defer syscall.FreeLibrary(Psapi)

	//var hProcess int32
	OpenProcess, err := syscall.GetProcAddress(Kernel, "OpenProcess")
	GetProcessMemoryInfo, err := syscall.GetProcAddress(Psapi, "GetProcessMemoryInfo")
	//GetProcessMemoryInfo, err := syscall.GetProcAddress(Kernel, "GetProcessMemoryInfo")
	if err != nil {
		panic(err)
	}
	var ptr uintptr
	var nargs_ptr uintptr = 3
	var arg1_ptr uintptr = READ_CONTROL | PROCESS_QUERY_INFORMATION | PROCESS_QUERY_LIMITED_INFORMATION
	var arg2_ptr uintptr = 0
	var arg3_ptr uintptr = pid
	fmt.Printf("\n ptr指针地址:   %d\n", ptr)
	//PROCESS_ALL_ACCESS or READ_CONTROL or ACCESS_SYSTEM_SECURITY or PROCESS_QUERY_INFORMATION
	var max_mem int32
	max_mem = 0
	for {
		select {
		case <-ch:
			fmt.Println("cancel goroutine by ch", max_mem)
			return
		default:
			ret, _, errn := syscall.Syscall(uintptr(OpenProcess), nargs_ptr, arg1_ptr, arg2_ptr, arg3_ptr)
			var hprocess uintptr = uintptr(ret)

			var SyncedBuffer SyncedBufferT
			var cb uintptr = uintptr(len(SyncedBuffer.buffer))
			var pro_mem_info uintptr = uintptr(unsafe.Pointer(&SyncedBuffer))

			//fmt.Printf("\n返回HWND:  %d\n", hProcess)
			ret, _, errn = syscall.Syscall(uintptr(GetProcessMemoryInfo), nargs_ptr, hprocess, pro_mem_info, cb)
			if errn != 0 {
				err = errn
				fmt.Println("Error !!!", err)
				return
			}
			var mem_used *int32
			mem_used = (*int32)(unsafe.Pointer(&SyncedBuffer.buffer[8]))
			//fmt.Printf("\n Mem_Used:  %d\n", *mem_used)
			if *mem_used > memLimit {
				os.Exit(utils.STATUS_MEMORY_LIMIT_EXCEEDED)
			}
			if *mem_used > max_mem {
				max_mem = *mem_used
			}
		}
	}
}
