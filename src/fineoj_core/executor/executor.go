package main

import (
	"os"
	"fmt"
	"../utils"
	"os/exec"
	"io"
	"bufio"
	"time"
	"sync"
)

func main() {
	//初始化
	RunDir := os.Args[1]
	RunFile := os.Args[2]
	InputFile := os.Args[3]
	OutputFile := os.Args[4]

	os.Chdir(RunDir)
	stdout, err := os.Create("judge_run.out")
	utils.PanicError(err)
	os.Stdout = stdout
	stderr, err := os.Create("judge_err.out")
	utils.PanicError(err)
	os.Stderr = stderr

	//copy data.in
	fmt.Println(InputFile)
	fmt.Println(RunDir)
	cmd := exec.Command("xcopy", InputFile, RunDir)
	err = cmd.Start()
	if err != nil {
		fmt.Println("error 0: ", err)
		os.Exit(utils.SYSERROR_FILE_PREPARE_ERROR)
	}
	err = cmd.Wait()
	if err != nil {
		fmt.Println("error 00: ", err)
		os.Exit(utils.SYSERROR_FILE_PREPARE_ERROR)
	}

	//开始运行用户程序
	var procAttr os.ProcAttr
	in, err := os.Open("data.in")
	if err != nil {
		fmt.Println("error 1: ", err)
		os.Exit(utils.SYSERROR_FILE_OPEN_ERROR)
	}
	defer in.Close()
	out, err := os.Create("user.out")
	if err != nil {
		fmt.Println("error 2: ", err)
		os.Exit(utils.SYSERROR_FILE_OPEN_ERROR)
	}
	defer out.Close()

	procAttr.Files = []*os.File{in, out, os.Stderr}
	args := make([]string,1)
	p, err := os.StartProcess(RunFile, args, &procAttr)
	if err != nil {
		fmt.Println("error 3: ", err)
		os.Exit(utils.SYSERROR_FILE_OPEN_ERROR)
	}
	ch := make(chan int, 1)
	//go mem_tool.CheckMemory(ch, uintptr(p.Pid), 256 * 1024 * 1024)
	fmt.Println(OutputFile)
	fmt.Println(p.Pid)
	var lock sync.RWMutex
	exited := false
	go func() {
		time.Sleep(time.Second)
		lock.RLock()
		if !exited {
			for p.Kill() != nil {
			}
			os.Exit(utils.STATUS_TIME_LIMIT_EXCEEDED)
		}
		lock.RUnlock()
	}()
	ps,err := p.Wait()
	ch <- 1
	lock.Lock()
	exited = true
	lock.Unlock()
	if !ps.Exited() {
		fmt.Println("error 4: ", err)
		os.Exit(utils.SYSERROR_WAIT4_ERROR)
	}
	if !ps.Success() {
		os.Exit(utils.STATUS_RUNTIME_ERROR)
	}
	if ps.SystemTime() > time.Second {
		os.Exit(utils.STATUS_TIME_LIMIT_EXCEEDED)
	}
	checkWA(RunDir+"\\user.out", OutputFile) //检查是否wa
	checkPE(RunDir+"\\user.out", OutputFile) //检查两文件是否相同
}

func checkPE(userout, answer string) {
	f1, err := os.Open(userout)
	if err != nil {
		fmt.Println("error 4: ", err)
		os.Exit(utils.SYSERROR_FILE_OPEN_ERROR)
	}
	defer f1.Close()
	f2, err := os.Open(answer)
	if err != nil {
		fmt.Println("error 5: ", err)
		os.Exit(utils.SYSERROR_FILE_OPEN_ERROR)
	}
	defer f2.Close()
	buff1 := bufio.NewReader(f1)
	buff2 := bufio.NewReader(f2)
	for {
		line1, err1 := buff1.ReadString('\n')
		if err1 != nil && err1 != io.EOF {
			fmt.Println("error 6: ", err)
			os.Exit(utils.SYSERROR_FILE_SCAN_ERROR)
		}
		line2, err2 := buff2.ReadString('\n')
		if err2 != nil && err2 != io.EOF {
			fmt.Println("error 7: ", err)
			os.Exit(utils.SYSERROR_FILE_SCAN_ERROR)
		}
		if err1 == err2 && err1 == io.EOF {
			break
		}
		fmt.Println("ddd line1", line1, "*")
		fmt.Println("ddd line2", line2, "*")
		if err1 != err2 || line1 != line2 {
			os.Exit(utils.STATUS_PRESENTATION)
		}
	}
}

func checkWA(userout, answer string) {
	f1, err := os.Open(userout)
	if err != nil {
		fmt.Println(err)
		os.Exit(utils.SYSERROR_FILE_OPEN_ERROR)
	}
	defer f1.Close()
	f2, err := os.Open(answer)
	if err != nil {
		fmt.Println(err)
		os.Exit(utils.SYSERROR_FILE_OPEN_ERROR)
	}
	defer f2.Close()
	w1 := new(string)
	w2 := new(string)
	for {
		n1, err1 := fmt.Fscan(f1, w1)
		if err1 != nil && err1 != io.EOF {
			fmt.Println(err)
			os.Exit(utils.SYSERROR_FILE_SCAN_ERROR)
		}
		n2, err2 := fmt.Fscan(f2, w2)
		if err2 != nil && err2 != io.EOF {
			fmt.Println(err)
			os.Exit(utils.SYSERROR_FILE_SCAN_ERROR)
		}
		if err1 == io.EOF && err2 == io.EOF {
			break
		}
		fmt.Println("CCC", n1, n2, *w1, *w2)
		if err1 != err2 || n1 != n2 || *w1 != *w2 {
			os.Exit(utils.STATUS_WRONG_ANSWER)
		}
	}
}
