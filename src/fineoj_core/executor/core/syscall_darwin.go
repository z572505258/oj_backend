package core

/*
说明：
Time Limit ：
	在execl之前使用setrlimit(RLIMIT_CPU)，事件超过限制之后，会收到一个信号SIGXCPU，然后手动kill
Mem Limit ：
	正常运行结束后，通过wait4获取进程使用的最大内存，但是这里获取的是rss已经写入的值，而不是total。比如用户开辟了一个数组但从来没有往数组中写入值，那么该数组的内存空间不计算在内
	[貌似这里去掉setrlimit对系统没啥影响?] 因为这里是在用户程序运行之后统计的，为防止内存过大影响操作系统正常运行，在execl之前使用setrlimit(RLIMIT_RSS)，如果内存超过了该限制值，那么操作系统会在内存不够用时从该进程中拿取内存
*/


/*
#include<sys/resource.h>
#include<unistd.h>
#include<signal.h>
#include <sys/types.h>
#include <sys/ptrace.h>
*/
import "C"
import (
	"syscall"
	"../../utils"
	"os"
	"fmt"
	"time"
	"unsafe"
)

func Fork() int {
	// About to call fork.
	// No more allocation or calls of non-assembly functions.
	r1, r2, err := syscall.RawSyscall(syscall.SYS_FORK, 0, 0, 0)
	utils.PanicErrno(err)

	// On Darwin:
	//	r1 = child pid in both parent and child.
	//	r2 = 0 in parent, 1 in child.
	// Convert to normal Unix r1 = 0 in child.
	if r2 == 1 {
		r1 = 0
	}
	return int(r1)
}

func Exec(argv0 string, argv []string) {
	argv0p, err := syscall.BytePtrFromString(argv0)
	utils.PanicError(err)
	argvp, err := syscall.SlicePtrFromStrings(argv)
	utils.PanicError(err)
	envvp, err := syscall.SlicePtrFromStrings(make([]string,0))
	utils.PanicError(err)
	_, _, err = syscall.RawSyscall(syscall.SYS_EXECVE,
		uintptr(unsafe.Pointer(argv0p)),
		uintptr(unsafe.Pointer(&argvp[0])),
		uintptr(unsafe.Pointer(&envvp[0])))
	utils.PanicError(err)
}

func WatchSolution(pid int, mem_limit_mb uint64) *syscall.Rusage {
	status := new(syscall.WaitStatus)
	rusage := new(syscall.Rusage)
	for {
		_,err := syscall.Wait4(pid, status, 0, rusage) //子进程开始执行

		fmt.Println(time.Now(),"User time", rusage.Utime.Sec * 1000 + int64(rusage.Utime.Usec/1000), "		Sys time", rusage.Stime.Sec * 1000 + int64(rusage.Stime.Usec/1000)) //ms
		fmt.Println(time.Now(),"Maxrss", rusage.Maxrss) //ms
		if err != nil {
			fmt.Println("syscall.Wait4: ",err)
			os.Exit(utils.SYSERROR_WAIT4_ERROR)
		}
		fmt.Println("status !!!:", *status)
		//fmt.Println(GetTopMemory(pid))
		if status.Stopped() { //子进程被暂停
			fmt.Println("StopSignal: ",int(status.StopSignal()))
			switch status.StopSignal(){
			case syscall.SIGXCPU:
				_,_,err := syscall.RawSyscall(syscall.SYS_PTRACE, uintptr(syscall.PTRACE_KILL), uintptr(pid), uintptr(1))
				utils.PanicErrno(err)
				fmt.Println("Time limit Exceeded")
				os.Exit(utils.STATUS_TIME_LIMIT_EXCEEDED)
			case syscall.SIGSEGV, syscall.SIGFPE, syscall.SIGBUS: //segmentation violation, floating point exception, 总线错误
				_,_,err := syscall.RawSyscall(syscall.SYS_PTRACE, uintptr(syscall.PTRACE_KILL), uintptr(pid), uintptr(1))
				utils.PanicErrno(err)
				fmt.Println("Runtime Error")
				os.Exit(utils.STATUS_RUNTIME_ERROR)
			default:
				fmt.Println("Unknown StopSignal: ", status.StopSignal())
			}
		} else if status.Exited() {
			if rusage.Maxrss > 0 && uint64(rusage.Maxrss) > mem_limit_mb * 1048576 {
				fmt.Println("Memory limit Exceeded")
				os.Exit(utils.STATUS_MEMORY_LIMIT_EXCEEDED)
			}
			if status.ExitStatus() !=0 {
				os.Exit(utils.SYSERROR_CHILD_UNDEFINED_EXIT_CODE)
			}
			//用户程序正确退出
			return rusage
		}
		fmt.Println(status.Signal())
		_,_,err1 := syscall.RawSyscall(syscall.SYS_PTRACE, uintptr(syscall.PTRACE_CONT), uintptr(pid), uintptr(1))
		utils.PanicErrno(err1)
	}
}

func Nice(p int) {
	err := syscall.Setpriority(syscall.PRIO_PROCESS, os.Getpid(), 19)
	utils.PanicError(err)
}

func SetTimeLimit(sec uint64) {
	limit := new(syscall.Rlimit)
	limit.Cur = sec
	limit.Max = sec
	err := syscall.Setrlimit(syscall.RLIMIT_CPU,limit)
	utils.PanicError(err)
}

func SetProcessNumLimit(num uint64) {
	limit := new(syscall.Rlimit)
	limit.Cur = num
	limit.Max = num
	err := syscall.Setrlimit(C.RLIMIT_NPROC,limit)
	utils.PanicError(err)
}

func SetOutputFileSize(mb uint64) {
	limit := new(syscall.Rlimit)
	limit.Cur = mb * 1048576
	limit.Max = mb * 1048576
	err := syscall.Setrlimit(syscall.RLIMIT_FSIZE,limit)
	utils.PanicError(err)
}

func SetMemoryLimit(mb uint64) {
	limit := new(syscall.Rlimit)
	limit.Cur = mb * 1048576
	limit.Max = mb * 1048576
	//err := syscall.Setrlimit(C.RLIMIT_STACK,limit)
	//utils.PanicError(err)
	err := syscall.Setrlimit(C.RLIMIT_RSS,limit)
	utils.PanicError(err)
}

func PtraceMe() {
	_, _, err := syscall.RawSyscall(syscall.SYS_PTRACE, uintptr(syscall.PTRACE_TRACEME), 0, 0)
	utils.PanicErrno(err)
}


func ChRoot(root *byte) {
	_, _, err := syscall.RawSyscall(syscall.SYS_CHROOT, uintptr(unsafe.Pointer(root)), 0, 0)
	utils.PanicErrno(err)
}