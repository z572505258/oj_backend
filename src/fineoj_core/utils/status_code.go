package utils


const (
	STATUS_ACCEPT = 0
	STATUS_TIME_LIMIT_EXCEEDED = 50
	STATUS_MEMORY_LIMIT_EXCEEDED = 51
	STATUS_RUNTIME_ERROR = 52
	STATUS_WRONG_ANSWER = 53
	STATUS_PRESENTATION = 54

	SYSERROR_WAIT4_ERROR = 70
	SYSERROR_CHILD_UNDEFINED_EXIT_CODE = 71
	SYSERROR_FILE_PREPARE_ERROR = 72
	SYSERROR_PTRACE_CONTINUE_ERROR = 73
	SYSERROR_FILE_OPEN_ERROR = 74
	SYSERROR_FILE_SCAN_ERROR = 75
)



func GetStatus(code int) string {
	switch code {
	case STATUS_ACCEPT:
		return "Accepted"
	case STATUS_TIME_LIMIT_EXCEEDED:
		return "Time Limit Exceeded"
	case STATUS_MEMORY_LIMIT_EXCEEDED:
		return "Memory Limit Exceeded"
	case STATUS_RUNTIME_ERROR:
		return "Runtime Error"
	case STATUS_WRONG_ANSWER:
		return "Wrong Answer"
	case STATUS_PRESENTATION:
		return "Presentation Error"
	default:
		return "System Error"
	}
}