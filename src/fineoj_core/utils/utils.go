package utils

import (
	"fmt"
	"os"
	"syscall"
)

func PanicErrno(err syscall.Errno) {
	if err!=0 {
		fmt.Println(err)
		os.Exit(9)
	}
}

func PanicError(err error) {
	if err!=nil {
		fmt.Println(err)
		os.Exit(9)
	}
}