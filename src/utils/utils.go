package utils

import (
	"regexp"
	"strings"
	"github.com/astaxie/beego"
	"os"
	"github.com/astaxie/beego/logs"
	"net/http"
	"io/ioutil"
	"../models"
	"fmt"
	"golang.org/x/text/transform"
	"golang.org/x/text/encoding/simplifiedchinese"
	"net/url"
	"time"
	"sync"
)

func PanicErr(err error) {
	if err != nil {
		logs.Warn(err)
		panic(err)
	}
}

func GetSubStr(text string, left string, right string) string {
	re := regexp.MustCompile(left + ".*" + right)
	match := re.FindAllString(text, 1)
	if match == nil {
		return ""
	} else {
		return match[0][len(left):len(match[0])-len(right)]
	}
}

func MatchSub(text string, pattern string, i int) string {
	matchs := regexp.MustCompile(pattern).FindStringSubmatch(text)
	if len(matchs) > i {
		return matchs[i]
	} else {
		return ""
	}
}

func MatchSubLeftMost(text string, pattern string) string {
	return MatchSub(text, pattern, 1)
}

var imageRootDir string
var picIdMu sync.RWMutex
func ProcessPicture(html, host, nowDir0, oj string) string {
	defer func() {
		err := recover()
		if err != nil {
			logs.Error(err)
			panic(err)
		}
	}()
	if nowDir0[len(nowDir0)-1] != '/' {
		nowDir0 += "/"
	}
	if nowDir0[0] != '/' {
		nowDir0 = "/" + nowDir0
	}
	if host[len(host)-1] == '/' {
		host = host[0:len(host)-1]
	}
	tags := regexp.MustCompile(`<img[\s\S^>]*?>`).FindAllString(html, -1)
	for i := 0; i < len(tags); i++ {
		nowDir := nowDir0
		rawUrl0 := MatchSub(tags[i], `<img[^<>]*src=(['" ]*)([\s\S]*?)['"]?([ >]| */>)`, 2)
		rawUrl := strings.Replace(rawUrl0, "\\", "/", -1) //替换反斜杠
		if rawUrl[0] == '/' {
			nowDir = "/"
		}
		absoluteUrl := ""
		if regexp.MustCompile(`:`).MatchString(rawUrl) { //图片跨域
			absoluteUrl = rawUrl
		} else {
			paths := strings.Split(rawUrl, "/")
			for i := 0; i < len(paths); i++ {
				if paths[i] == "." {
					continue
				} else if paths[i] == ".." { //上一级目录
					if nowDir == "/" {
						continue;
					} else {
						nowDir = MatchSubLeftMost(nowDir, `(.*/)[^/]*?/$`)
					}
				} else if len(paths[i]) > 0 {
					nowDir += paths[i] + "/"
				}
			}
			absoluteUrl = host + nowDir[0:len(nowDir)-1]
		}
		var suffix string //后缀名
		suffix = regexp.MustCompile(`\.[a-zA-z]*$`).FindAllString(absoluteUrl, 1)[0]
		//获取图片
		rsp, err := http.Get(absoluteUrl)
		PanicErr(err)
		if rsp.StatusCode == 404 {
			logs.Warn("picture not found absoluteUrl=[%s]", absoluteUrl)
			//html = strings.Replace(html, rawUrl0, absoluteUrl, -1)
			continue
		}
		imgByte, err := ioutil.ReadAll(rsp.Body)
		rsp.Body.Close()
		//保存图片
		var getLocalPicId = func() string {
			picIdMu.Lock()
			defer picIdMu.Unlock()
			var localPicId string
			rows, err := models.DB.Query("select local from pic_path_map where url = ?", absoluteUrl)
			PanicErr(err)
			if rows.Next() {
				rows.Scan(&localPicId)
				rows.Close()
			} else {
				rows.Close()
				ret, err := models.DB.Exec("insert into pic_path_map(url) values (?)", absoluteUrl)
				PanicErr(err)
				i, err := ret.LastInsertId()
				PanicErr(err)
				localPicId = fmt.Sprintf("%d", i)
			}
			return localPicId
		}
		localPicName := getLocalPicId() + suffix
		//os.MkdirAll(MatchSubLeftMost(local_pic_path + "/", `(.*/)[^/]*?/$`), os.ModePerm)
		var picFile *os.File
		picFile, err = os.Create(imageRootDir + "/remoteoj/" + localPicName)
		PanicErr(err)
		_, err = picFile.Write(imgByte)
		picFile.Close()
		PanicErr(err)
		//替换html
		html = strings.Replace(html, rawUrl0, "images/remoteoj/"+localPicName, -1)
		logs.Info("tag=[%s] raw_url0=[%s] raw_url=[%s] absolute_url=[%s] local_pic_name=[%s]", tags[i], rawUrl0, rawUrl, absoluteUrl, localPicName)
	}
	return html
}

func MatchSubLeftMostProPic(html, pattern, host, nowdir, oj string) string {
	html = MatchSub(html, pattern, 1)
	return ProcessPicture(html, host, nowdir, oj)
}

func MatchSubProPic(html, pattern, host, nowdir, oj string, i int) string {
	html = MatchSub(html, pattern, i)
	return ProcessPicture(html, host, nowdir, oj)
}

func init() {
	imageRootDir = beego.AppConfig.String("image_root_dir")
	if imageRootDir[len(imageRootDir)-1] != '/' {
		imageRootDir += "/"
	}
	_, err := os.Stat(imageRootDir)
	if err != nil {
		logs.Error("pic_root_dir=[%s] not exist", imageRootDir)
		os.Exit(2)
	}
	os.Mkdir(imageRootDir+"/remoteoj", os.ModePerm)
}

func ProcessTr(tr string) []string {
	tdContents := regexp.MustCompile(`<td[\s\S^<>]*?>([\s\S]*?)</td>`).FindAllString(tr, -1);
	if tdContents == nil {
		panic("utils.ProcessTr() cannot find td")
	}
	l := len(tdContents)
	re := regexp.MustCompile(`<[\s\S^<>]*?>`)
	reL := regexp.MustCompile(`^\s*`)
	reR := regexp.MustCompile(`\s*$`)
	for i := 0; i < l; i++ {
		tdContents[i] = re.ReplaceAllString(tdContents[i], "") //去掉所有html标签
		tdContents[i] = reL.ReplaceAllString(tdContents[i], "")
		tdContents[i] = reR.ReplaceAllString(tdContents[i], "")
	}
	//logs.Debug(tdContents)
	return tdContents
}

//清除html所有标签
func ClearTags(src string) string {
	return string(regexp.MustCompile(`<[^>]*>`).ReplaceAll([]byte(src), nil))
}

func HtmlToNormal(html string) string {
	html = strings.Replace(html, `&lt;`, `<`, -1)
	html = strings.Replace(html, `&gt;`, `>`, -1)
	html = strings.Replace(html, `&amp;`, `&`, -1)
	return strings.Replace(html, `&nbsp;`, ` `, -1)
}

func ClientGet(c *http.Client, url string) string {
	rsp, err := c.Get(url)
	PanicErr(err)
	defer rsp.Body.Close()
	body, err := ioutil.ReadAll(rsp.Body)
	PanicErr(err)
	return string(body[:])
}

func ClientGetGBK(c *http.Client, url string) string {
	rsp, err := c.Get(url)
	PanicErr(err)
	defer rsp.Body.Close()
	body, err := ioutil.ReadAll(transform.NewReader(rsp.Body, simplifiedchinese.GBK.NewDecoder()))
	PanicErr(err)
	return string(body[:])
}

func ClientPost(c *http.Client, form url.Values, url string) string {
	rsp, err := c.PostForm(url, form)
	PanicErr(err)
	defer rsp.Body.Close()
	body, err := ioutil.ReadAll(transform.NewReader(rsp.Body, simplifiedchinese.GBK.NewDecoder()))
	PanicErr(err)
	return string(body[:])
}

func GetRootPath() string {
	return beego.AppConfig.String("rootpath")
}


func init() {
	http.DefaultClient.Timeout = time.Second * 10
}